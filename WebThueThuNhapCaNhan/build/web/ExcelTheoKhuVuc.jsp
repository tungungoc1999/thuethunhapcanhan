<%@page import="entity.ThongKeTheoKhuVuc"%>
<%@page import="entity.DanhSachThuThue"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.List" %>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   </head>
   <body>
      <h1>Danh sách tiền đóng thống kê theo khu vực</h1>
      <table cellpadding="1"  cellspacing="1" border="1" bordercolor="gray">
         <tr>
             <td style="text-align:center">STT</td>
             <td style="text-align:center">Tên khu vực</td>
             <td style="text-align:center">Tháng </td>
             <td style="text-align:center">Năm đóng</td>
 <td style="text-align:center">Tổng tiền</td>
         </tr>
         <%
            List<ThongKeTheoKhuVuc> lst  = (List<ThongKeTheoKhuVuc>)request.getAttribute("DSTheoKhuVuc");
                  if (lst != null) {
                      response.setContentType("application/vnd.ms-excel");
                      response.setHeader("Content-Disposition", "inline; filename="+ "Danhsach.xls");
                  }
            int i = 1;
            for(ThongKeTheoKhuVuc e: lst){
            %>
         <tr>
             <td><%= i++%></td>
             <td><%=e.getTenKhuVuc()%></td>
             <td><%=e.getThangDong()%></td>
             <td><%=e.getNamDong()%></td>
             <td><%=e.getTongTien()%></td>
             
         </tr>
         <% 
             }
i=1;
            %>
      </table>
   </body>
</html>