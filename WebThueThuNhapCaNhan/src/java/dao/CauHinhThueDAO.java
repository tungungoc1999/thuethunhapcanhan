/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.ConnectDB;
import entity.CauHinhThue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tran Tu
 */
public class CauHinhThueDAO {
ConnectDB db = new ConnectDB();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    String queryLast = "SELECT * FROM CauHinhThue WHERE ID = (SELECT MAX(ID) FROM CauHinhThue)";

    public List<CauHinhThue> getAllCauHinhThue() {
        List<CauHinhThue> lst = new ArrayList<>();
        String query = "select * from CauHinhThue";
        try {
            conn = db.getConnection();//ket noi
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                lst.add(new CauHinhThue(rs.getInt(1), rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6)));
            }
        } catch (Exception e) {
        }

        return lst;
    }

    public CauHinhThue findById(String id) {
        CauHinhThue result = null;
        String query = "Select  * from CauHinhThue\n"
                + "where ID = ?";
        try {
            conn = db.getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                result = new CauHinhThue(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6));
            }
        } catch (Exception e) {
            result = null;
        }
        return result;
    }
    
    public CauHinhThue findLast() {
        CauHinhThue result = null;
        try {
            conn = db.getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(queryLast);
            rs = ps.executeQuery();
            while (rs.next()) {
                result = new CauHinhThue(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6));
            }
        } catch (Exception e) {
            result = null;
        }
        return result;
    }
    

    public boolean createCauHinhThue(String bacthue, String tntt, String thuexuat, String cachtinh1, String cachtinh2) {
        String query = "INSERT INTO CauHinhThue\n"
                + " VALUES (?,?,?,?,?)";
        try {
            conn = db.getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, bacthue);
            ps.setString(2, tntt);
            ps.setString(3, thuexuat);
            ps.setString(4, cachtinh1);
            ps.setString(5, cachtinh2);
            int count = ps.executeUpdate();
            if (count > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public boolean createCauHinhThue(CauHinhThue item) {
        String query = "INSERT INTO CauHinhThue\n"
                + " VALUES (?,?,?,?,?)";
        try {
            conn = db.getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setInt(1, item.getBacThue());
            ps.setString(2, item.getThuNhapTinhThue());
            ps.setInt(3, item.getThueXuat());
            ps.setString(4, item.getCachTinhThue1());
            ps.setString(5, item.getCachTinhThue2());
            int count = ps.executeUpdate();
            if (count > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean updateCauHinhThue(String ID, String bacthue, String tntt, String thuexuat, String cachtinh1, String cachtinh2) {
        String query = "UPDATE CauHinhThue\n"
                + "   SET BacThue = ?\n"
                + "      ,ThuNhapTinhThue = ?\n"
                + "      ,ThueXuat = ?\n"
                + "      ,CachTinhThue1 = ?\n"
                + "      ,CachTinhThue2 = ?\n"
                + " WHERE ID = ?";
        try {
            conn = db.getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, bacthue);
            ps.setString(2, tntt);
            ps.setString(3, thuexuat);
            ps.setString(4, cachtinh1);
            ps.setString(5, cachtinh2);
            ps.setString(6, ID);
            int count = ps.executeUpdate();
            if(count > 0){
                return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean updateCauHinhThue(CauHinhThue item) {
        String query = "UPDATE CauHinhThue\n"
                + "   SET BacThue = ?\n"
                + "      ,ThuNhapTinhThue = ?\n"
                + "      ,ThueXuat = ?\n"
                + "      ,CachTinhThue1 = ?\n"
                + "      ,CachTinhThue2 = ?\n"
                + " WHERE ID = ?";
        try {
            conn = db.getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setInt(1, item.getBacThue());
            ps.setString(2, item.getThuNhapTinhThue());
            ps.setInt(3, item.getThueXuat());
            ps.setString(4, item.getCachTinhThue1());
            ps.setString(5, item.getCachTinhThue2());
            ps.setInt(6, item.getID());
            int count = ps.executeUpdate();
            if(count > 0){
                return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean deleteCauHinhThue(String ID) {
        String query = "delete from CauHinhThue\n"
                + "where ID = ?";
        try {
            conn = db.getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.setString(1, ID);
            int count = ps.executeUpdate();
            if(count > 0){
                return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
}

