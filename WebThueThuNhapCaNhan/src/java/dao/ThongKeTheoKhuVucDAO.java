/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.ConnectDB;
import entity.ThongKeTheoKhuVuc;
import entity.TinhThanh;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Tran Tu
 */
public class ThongKeTheoKhuVucDAO {

    ConnectDB db = new ConnectDB();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public List<ThongKeTheoKhuVuc> getAllThongKeTheoKhuVuc() {
        List<ThongKeTheoKhuVuc> lst = new ArrayList<>();
        String query = "select * from DanhSachDongTienTheoKhuVuc";
        try {
            conn = db.getConnection();//ket noi
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                lst.add(new ThongKeTheoKhuVuc(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6)));
            }
        } catch (Exception e) {
        }

        return lst;
    }
    public List<ThongKeTheoKhuVuc> getAllThongKeTheoKhuVuc(String khuvuc, String thang,String nam) {
        List<ThongKeTheoKhuVuc> lst = new ArrayList<>();
        String query = "select * from DanhSachDongTienTheoKhuVuc where ";
        String query1 = "KhuVuc = "+khuvuc;
        String query2 = "Thang = "+thang + " and Nam = "+ nam;
        try {
            if(khuvuc.equals("0")){
                query = query + query2;
            }else if(thang.equals("0")){
                query = query + query1;
            }else{
                query = query + query1 + " and " + query2;
            }
            conn = db.getConnection();//ket noi
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                lst.add(new ThongKeTheoKhuVuc(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6)));
            }
        } catch (Exception e) {
        }

        return lst;
    }
//    public boolean createCauHinhThue(String bacthue, String tntt, String thuexuat, String cachtinh1, String cachtinh2) {
//        String query = "INSERT INTO DanhSachDongTienTheoKhuVuc\n"
//                + " VALUES (?,?,?,?,?)";
//        try {
//            conn = db.getConnection();//mo ket noi voi sql
//            ps = conn.prepareStatement(query);
//            ps.setString(1, bacthue);
//            ps.setString(2, tntt);
//            ps.setString(3, thuexuat);
//            ps.setString(4, cachtinh1);
//            ps.setString(5, cachtinh2);
//            int count = ps.executeUpdate();
//            if (count > 0) {
//                return true;
//            } else {
//                return false;
//            }
//        } catch (Exception e) {
//            return false;
//        }
//    }
//    public static void main(String[] args) {
//        List<TinhThanh> lst = new TinhThanhDAO().getAllTinhThanh();
//        Random generator = new Random();
//       ThongKeTheoKhuVucDAO x = new ThongKeTheoKhuVucDAO();
//        for (TinhThanh tinhThanh : lst) {
//             int value = generator.nextInt(100) *10000000;
//             x.createCauHinhThue(String.valueOf(tinhThanh.getMaTP()), tinhThanh.getTenTinh(), "2", "2021", String.valueOf(value));
//        }
//    }
}
