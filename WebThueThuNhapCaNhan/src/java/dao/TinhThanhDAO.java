/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.ConnectDB;
import entity.TinhThanh;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tran Tu
 */
public class TinhThanhDAO {
    
    ConnectDB db = new ConnectDB();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    public List<TinhThanh> getAllTinhThanh() {
        List<TinhThanh> lst = new ArrayList<>();
        String query = "select * from Tinh_Thanh";
        try {
            conn = db.getConnection();//ket noi
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                lst.add(new TinhThanh(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4)));
            }
        } catch (Exception e) {
        }

        return lst;
    }
}
