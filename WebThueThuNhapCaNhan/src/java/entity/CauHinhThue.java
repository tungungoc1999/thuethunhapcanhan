/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Objects;

/**
 *
 * @author Hp
 */
public class CauHinhThue {
    private int ID;
    private int BacThue;
    private String ThuNhapTinhThue;
    private int ThueXuat;
    private String CachTinhThue1;
    private String CachTinhThue2;

    public CauHinhThue() {
    }
    public CauHinhThue(int BacThue, String ThuNhapTinhThue, int ThueXuat, String CachTinhThue1, String CachTinhThue2) {
        this.BacThue = BacThue;
        this.ThuNhapTinhThue = ThuNhapTinhThue.trim();
        this.ThueXuat = ThueXuat;
        this.CachTinhThue1 = CachTinhThue1.trim();
        this.CachTinhThue2 = CachTinhThue2.trim();
    }
    public CauHinhThue(int ID, int BacThue, String ThuNhapTinhThue, int ThueXuat, String CachTinhThue1, String CachTinhThue2) {
        this.ID = ID;
        this.BacThue = BacThue;
        this.ThuNhapTinhThue = ThuNhapTinhThue.trim();
        this.ThueXuat = ThueXuat;
        this.CachTinhThue1 = CachTinhThue1.trim();
        this.CachTinhThue2 = CachTinhThue2.trim();
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getBacThue() {
        return BacThue;
    }

    public void setBacThue(int BacThue) {
        this.BacThue = BacThue;
    }

    public String getThuNhapTinhThue() {
        return ThuNhapTinhThue.trim();
    }

    public void setThuNhapTinhThue(String ThuNhapTinhThue) {
        this.ThuNhapTinhThue = ThuNhapTinhThue.trim();
    }

    public int getThueXuat() {
        return ThueXuat;
    }

    public void setThueXuat(int ThueXuat) {
        this.ThueXuat = ThueXuat;
    }

    public String getCachTinhThue1() {
        return CachTinhThue1.trim();
    }

    public void setCachTinhThue1(String CachTinhThue1) {
        this.CachTinhThue1 = CachTinhThue1.trim();
    }

    public String getCachTinhThue2() {
        return CachTinhThue2.trim();
    }

    public void setCachTinhThue2(String CachTinhThue2) {
        this.CachTinhThue2 = CachTinhThue2.trim();
    }

    @Override
    public String toString() {
        return "CauHinhThue{" + "ID=" + ID + ", BacThue=" + BacThue + ", ThuNhapTinhThue=" + ThuNhapTinhThue + ", ThueXuat=" + ThueXuat + ", CachTinhThue1=" + CachTinhThue1 + ", CachTinhThue2=" + CachTinhThue2 + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CauHinhThue other = (CauHinhThue) obj;
        if (this.ID != other.ID) {
            return false;
        }
        if (this.BacThue != other.BacThue) {
            return false;
        }
        if (this.ThueXuat != other.ThueXuat) {
            return false;
        }
        if (!Objects.equals(this.ThuNhapTinhThue, other.ThuNhapTinhThue)) {
            return false;
        }
        if (!Objects.equals(this.CachTinhThue1, other.CachTinhThue1)) {
            return false;
        }
        if (!Objects.equals(this.CachTinhThue2, other.CachTinhThue2)) {
            return false;
        }
        return true;
    }
    
    
}
