/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

/**
 *
 * @author Hp
 */
public class DanhSachThuThue {
    public int ID;
    public String SoThue;
    public String HoTen;
    public int SoCMND;
    public String ThongTin;
    public String DiaChi;
    public String ThoiGianDongThue;
    public String ThuNhapTinhThue;
    public String SoTienDongThue;
    public String BacThue;
    public String ThangDong;
    public String NamDong;
    public String UpdateBy;
    public String TrangThai;
    public String KhuVuc;
    public String TenTrangThai;
    public String TenKhuVuc;

    public DanhSachThuThue() {
    }

    public DanhSachThuThue(int ID, String SoThue, String HoTen, int SoCMND, String ThongTin, String DiaChi, String ThoiGianDongThue, String ThuNhapTinhThue, String SoTienDongThue, String BacThue, String ThangDong, String NamDong, String UpdateBy, String TrangThai, String KhuVuc, String TenTrangThai, String TenKhuVuc) {
        this.ID = ID;
        this.SoThue = SoThue;
        this.HoTen = HoTen;
        this.SoCMND = SoCMND;
        this.ThongTin = ThongTin;
        this.DiaChi = DiaChi;
        this.ThoiGianDongThue = ThoiGianDongThue;
        this.ThuNhapTinhThue = ThuNhapTinhThue;
        this.SoTienDongThue = SoTienDongThue;
        this.BacThue = BacThue;
        this.ThangDong = ThangDong;
        this.NamDong = NamDong;
        this.UpdateBy = UpdateBy;
        this.TrangThai = TrangThai;
        this.KhuVuc = KhuVuc;
        this.TenTrangThai = TenTrangThai;
        this.TenKhuVuc = TenKhuVuc;
    }

    

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getSoThue() {
        return SoThue;
    }

    public void setSoThue(String SoThue) {
        this.SoThue = SoThue;
    }

    public String getHoTen() {
        return HoTen;
    }

    public void setHoTen(String HoTen) {
        this.HoTen = HoTen;
    }

    public int getSoCMND() {
        return SoCMND;
    }

    public void setSoCMND(int SoCMND) {
        this.SoCMND = SoCMND;
    }

    public String getThongTin() {
        return ThongTin;
    }

    public void setThongTin(String ThongTin) {
        this.ThongTin = ThongTin;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public void setDiaChi(String DiaChi) {
        this.DiaChi = DiaChi;
    }

    public String getThoiGianDongThue() {
        return ThoiGianDongThue;
    }

    public void setThoiGianDongThue(String ThoiGianDongThue) {
        this.ThoiGianDongThue = ThoiGianDongThue;
    }

    public String getThuNhapTinhThue() {
        if(ThuNhapTinhThue == null){
            ThuNhapTinhThue = "0";
        }
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        
        return formatter.format(Double.parseDouble(ThuNhapTinhThue))+" VNĐ";
    }

    public void setThuNhapTinhThue(String ThuNhapTinhThue) {
        this.ThuNhapTinhThue = ThuNhapTinhThue;
    }

    public String getSoTienDongThue() {
        if(SoTienDongThue == null){
            SoTienDongThue = "0";
        }
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(Double.parseDouble(SoTienDongThue))+" VNĐ";
    }

    public void setSoTienDongThue(String SoTienDongThue) {
        this.SoTienDongThue = SoTienDongThue;
    }

    public String getBacThue() {
        return BacThue;
    }

    public void setBacThue(String BacThue) {
        this.BacThue = BacThue;
    }

    public String getThangDong() {
        return ThangDong;
    }

    public void setThangDong(String ThangDong) {
        this.ThangDong = ThangDong;
    }

    public String getNamDong() {
        return NamDong;
    }

    public void setNamDong(String NamDong) {
        this.NamDong = NamDong;
    }

    public String getUpdateBy() {
        return UpdateBy;
    }

    public void setUpdateBy(String UpdateBy) {
        this.UpdateBy = UpdateBy;
    }

    public String getTrangThai() {
        return TrangThai;
    }

    public void setTrangThai(String TrangThai) {
        this.TrangThai = TrangThai;
    }

    public String getKhuVuc() {
        return KhuVuc;
    }

    public void setKhuVuc(String KhuVuc) {
        this.KhuVuc = KhuVuc;
    }

    public String getTenTrangThai() {
        return TenTrangThai;
    }

    public void setTenTrangThai(String TenTrangThai) {
        this.TenTrangThai = TenTrangThai;
    }

    public String getTenKhuVuc() {
        return TenKhuVuc;
    }

    public void setTenKhuVuc(String TenKhuVuc) {
        this.TenKhuVuc = TenKhuVuc;
    }

    @Override
    public String toString() {
        return "DanhSachThuThue{" + "ID=" + ID + ", SoThue=" + SoThue + ", HoTen=" + HoTen + ", SoCMND=" + SoCMND + ", ThongTin=" + ThongTin + ", DiaChi=" + DiaChi + ", ThoiGianDongThue=" + ThoiGianDongThue + ", ThuNhapTinhThue=" + ThuNhapTinhThue + ", SoTienDongThue=" + SoTienDongThue + ", BacThue=" + BacThue + ", ThangDong=" + ThangDong + ", NamDong=" + NamDong + ", UpdateBy=" + UpdateBy + ", TrangThai=" + TrangThai + ", KhuVuc=" + KhuVuc + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DanhSachThuThue other = (DanhSachThuThue) obj;
        if (this.ID != other.ID) {
            return false;
        }
        if (this.SoCMND != other.SoCMND) {
            return false;
        }
        if (!Objects.equals(this.SoThue, other.SoThue)) {
            return false;
        }
        if (!Objects.equals(this.HoTen, other.HoTen)) {
            return false;
        }
        if (!Objects.equals(this.ThongTin, other.ThongTin)) {
            return false;
        }
        if (!Objects.equals(this.DiaChi, other.DiaChi)) {
            return false;
        }
        if (!Objects.equals(this.ThoiGianDongThue, other.ThoiGianDongThue)) {
            return false;
        }
        if (!Objects.equals(this.ThuNhapTinhThue, other.ThuNhapTinhThue)) {
            return false;
        }
        if (!Objects.equals(this.SoTienDongThue, other.SoTienDongThue)) {
            return false;
        }
        if (!Objects.equals(this.BacThue, other.BacThue)) {
            return false;
        }
        if (!Objects.equals(this.ThangDong, other.ThangDong)) {
            return false;
        }
        if (!Objects.equals(this.NamDong, other.NamDong)) {
            return false;
        }
        if (!Objects.equals(this.UpdateBy, other.UpdateBy)) {
            return false;
        }
        if (!Objects.equals(this.TrangThai, other.TrangThai)) {
            return false;
        }
        if (!Objects.equals(this.KhuVuc, other.KhuVuc)) {
            return false;
        }
        if (!Objects.equals(this.TenTrangThai, other.TenTrangThai)) {
            return false;
        }
        if (!Objects.equals(this.TenKhuVuc, other.TenKhuVuc)) {
            return false;
        }
        return true;
    }
    
    
}
