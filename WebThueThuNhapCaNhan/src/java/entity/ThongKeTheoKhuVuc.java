/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.text.DecimalFormat;
import java.util.Objects;

/**
 *
 * @author Tran Tu
 */
public class ThongKeTheoKhuVuc {

    public int ID;
    private int idKhuVuc;
    private String tenKhuVuc;
    public String ThangDong;
    public String NamDong;
    public String TongTien;

    @Override
    public String toString() {
        return "ThongKeTheoKhuVuc{" + "ID=" + ID + ", idKhuVuc=" + idKhuVuc + ", tenKhuVuc=" + tenKhuVuc + ", ThangDong=" + ThangDong + ", NamDong=" + NamDong + ", TongTien=" + TongTien + '}';
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getIdKhuVuc() {
        return idKhuVuc;
    }

    public void setIdKhuVuc(int idKhuVuc) {
        this.idKhuVuc = idKhuVuc;
    }

    public String getTenKhuVuc() {
        return tenKhuVuc;
    }

    public void setTenKhuVuc(String tenKhuVuc) {
        this.tenKhuVuc = tenKhuVuc;
    }

    public String getThangDong() {
        return ThangDong;
    }

    public void setThangDong(String ThangDong) {
        this.ThangDong = ThangDong;
    }

    public String getNamDong() {
        return NamDong;
    }

    public void setNamDong(String NamDong) {
        this.NamDong = NamDong;
    }

    public String getTongTien() {
        if(TongTien == null){
            TongTien = "0";
        }
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(Double.parseDouble(TongTien))+" VNĐ";
    }

    public void setTongTien(String TongTien) {
        this.TongTien = TongTien;
    }

    public ThongKeTheoKhuVuc() {
    }

    public ThongKeTheoKhuVuc(int ID, int idKhuVuc, String tenKhuVuc, String ThangDong, String NamDong, String TongTien) {
        this.ID = ID;
        this.idKhuVuc = idKhuVuc;
        this.tenKhuVuc = tenKhuVuc;
        this.ThangDong = ThangDong;
        this.NamDong = NamDong;
        this.TongTien = TongTien;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ThongKeTheoKhuVuc other = (ThongKeTheoKhuVuc) obj;
        if (this.ID != other.ID) {
            return false;
        }
        if (this.idKhuVuc != other.idKhuVuc) {
            return false;
        }
        if (!Objects.equals(this.tenKhuVuc, other.tenKhuVuc)) {
            return false;
        }
        if (!Objects.equals(this.ThangDong, other.ThangDong)) {
            return false;
        }
        if (!Objects.equals(this.NamDong, other.NamDong)) {
            return false;
        }
        if (!Objects.equals(this.TongTien, other.TongTien)) {
            return false;
        }
        return true;
    }
    
}
