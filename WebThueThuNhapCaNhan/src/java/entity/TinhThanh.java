/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Objects;

/**
 *
 * @author Tran Tu
 */
public class TinhThanh {
    private int ID;
    private int MaTP;
    private String TenTinh;
    private String Cap;

    public TinhThanh() {
    }

    public TinhThanh(int ID, int MaTP, String TenTinh, String Cap) {
        this.ID = ID;
        this.MaTP = MaTP;
        this.TenTinh = TenTinh;
        this.Cap = Cap;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getMaTP() {
        return MaTP;
    }

    public void setMaTP(int MaTP) {
        this.MaTP = MaTP;
    }

    public String getTenTinh() {
        return TenTinh;
    }

    public void setTenTinh(String TenTinh) {
        this.TenTinh = TenTinh;
    }

    public String getCap() {
        return Cap;
    }

    public void setCap(String Cap) {
        this.Cap = Cap;
    }

    @Override
    public String toString() {
        return "TinhThanh{" + "ID=" + ID + ", MaTP=" + MaTP + ", TenTinh=" + TenTinh + ", Cap=" + Cap + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TinhThanh other = (TinhThanh) obj;
        if (this.ID != other.ID) {
            return false;
        }
        if (this.MaTP != other.MaTP) {
            return false;
        }
        if (!Objects.equals(this.TenTinh, other.TenTinh)) {
            return false;
        }
        if (!Objects.equals(this.Cap, other.Cap)) {
            return false;
        }
        return true;
    }
    
}
