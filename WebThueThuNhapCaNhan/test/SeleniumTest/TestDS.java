/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SeleniumTest;

import dao.RollBackDB;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

/**
 *
 * @author Tran Tu
 */
//22
public class TestDS {
     private static WebDriver wd = null;
    public static RollBackDB rb = new RollBackDB();

    public TestDS() {

    }

    @BeforeClass
    public static void setUpClass() {
        System.setProperty("webdriver.chrome.driver", "D:/Tu/Netbean/Git_BTL_TTNCN/selenium/chromedriver.exe");
        wd = new ChromeDriver();
    }

    @AfterClass
    public static void tearDownClass() {
        rb.rollBackCauHinhThue();
    }

    @Before
    public void setUp() {
        wd.navigate().refresh();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testDSDaDongCoDuLieu() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dsdadong");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(1);
        Thread.sleep(1000);
        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
        ddlThang.selectByVisibleText("Tháng 5");
        Thread.sleep(1000);
        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Tìm kiếm thành công");
    }
    @Test
    public void testDSDaDongKhongCoDuLieu() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dsdadong");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(2);
        Thread.sleep(1000);
        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
        ddlThang.selectByVisibleText("Tháng 5");
        Thread.sleep(1000);
        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Không có dữ liệu phù hợp");
    }
    @Test
    public void testDSDaDongChuaChonKhuVuc() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dsdadong");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
//        ddlKhuVuc.selectByIndex(1);
        Thread.sleep(1000);
        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
        ddlThang.selectByVisibleText("Tháng 5");
        Thread.sleep(1000);
        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn chưa chọn khu vực");
    }
    @Test
    public void testDSDaDongChuaChonThang() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dsdadong");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(1);
        Thread.sleep(1000);
        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
//        ddlThang.selectByVisibleText("Tháng 5");
        Thread.sleep(1000);
        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn chưa chọn tháng đóng");
    }
    @Test
    public void testDSDaDongChuaChonNam() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dsdadong");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(1);
        Thread.sleep(1000);
        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
        ddlThang.selectByVisibleText("Tháng 5");
        Thread.sleep(1000);
        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
//        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn chưa chọn năm");
    }
    @Test
    public void testDSChuaDongCoDuLieu() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dschuadong");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(1);
        Thread.sleep(1000);
        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
        ddlThang.selectByVisibleText("Tháng 6");
        Thread.sleep(1000);
        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Tìm kiếm thành công");
    }
    @Test
    public void testDSChuaDongKhongCoDuLieu() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dschuadong");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(1);
        Thread.sleep(1000);
        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
        ddlThang.selectByVisibleText("Tháng 5");
        Thread.sleep(1000);
        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Không có dữ liệu phù hợp");
    }
    @Test
    public void testDSChuaDongChuaChonKhuVuc() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dschuadong");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
//        ddlKhuVuc.selectByIndex(1);
        Thread.sleep(1000);
        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
        ddlThang.selectByVisibleText("Tháng 5");
        Thread.sleep(1000);
        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn chưa chọn khu vực");
    }
    @Test
    public void testDSChuaDongChuaChonThang() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dschuadong");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(1);
        Thread.sleep(1000);
        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
//        ddlThang.selectByVisibleText("Tháng 5");
        Thread.sleep(1000);
        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn chưa chọn tháng đóng");
    }
    @Test
    public void testDSChuaDongChuaChonNam() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dschuadong");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(1);
        Thread.sleep(1000);
        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
        ddlThang.selectByVisibleText("Tháng 5");
        Thread.sleep(1000);
        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
//        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn chưa chọn năm");
    }
    
    @Test
    public void testDSTheoKhuVucCoDuLieu() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dstheokhuvuc");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(1);
        Thread.sleep(1000);
        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
        ddlThang.selectByVisibleText("Tháng 5");
        Thread.sleep(1000);
        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Tìm kiếm thành công");
    }
    @Test
    public void testDSTheoKhuVucKhongCoDuLieu() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dstheokhuvuc");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(2);
        Thread.sleep(1000);
        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
        ddlThang.selectByVisibleText("Tháng 5");
        Thread.sleep(1000);
        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
        ddlNam.selectByVisibleText("Năm 2015");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Không có dữ liệu phù hợp");
    }
    @Test
    public void testDSTheoKhongChon() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dstheokhuvuc");
//        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
//        ddlKhuVuc.selectByIndex(1);
//        Thread.sleep(1000);
//        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
//        ddlThang.selectByVisibleText("Tháng 5");
//        Thread.sleep(1000);
//        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
//        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn phải chọn khu vực hoặc chọn tháng và năm");
    }
    @Test
    public void testDSTheoChonNam() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dstheokhuvuc");
//        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
//        ddlKhuVuc.selectByIndex(1);
//        Thread.sleep(1000);
//        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
//        ddlThang.selectByVisibleText("Tháng 5");
//        Thread.sleep(1000);
        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn phải chọn khu vực hoặc chọn tháng và năm");
    }
    @Test
    public void testDSTheoChonThang() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dstheokhuvuc");
//        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
//        ddlKhuVuc.selectByIndex(1);
//        Thread.sleep(1000);
        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
        ddlThang.selectByVisibleText("Tháng 5");
//        Thread.sleep(1000);
//        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
//        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn phải chọn khu vực hoặc chọn tháng và năm");
    }
    @Test
    public void testDSTheoChonKhuVuc() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dstheokhuvuc");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(1);
//        Thread.sleep(1000);
//        Select ddlThang = new Select(wd.findElement(By.id("ddlThang")));
//        ddlThang.selectByVisibleText("Tháng 5");
//        Thread.sleep(1000);
//        Select ddlNam = new Select(wd.findElement(By.id("ddlNam")));
//        ddlNam.selectByVisibleText("Năm 2021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Tìm kiếm thành công");
    }
    
    @Test
    public void testDSTheoThoiGianCoDuLieu() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dstheothoigian");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(1);
        Thread.sleep(1000);
        WebElement startAt = wd.findElement(By.id("startAt"));
        startAt.sendKeys("03042021");
        Thread.sleep(1000);
        WebElement startEnd = wd.findElement(By.id("startEnd"));
        startEnd.sendKeys("03062021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Tìm kiếm thành công");
    }
    @Test
    public void testDSTheoThoiGianKhongCoDuLieu() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dstheothoigian");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(2);
        Thread.sleep(1000);
        WebElement startAt = wd.findElement(By.id("startAt"));
        startAt.sendKeys("03042021");
        Thread.sleep(1000);
        WebElement startEnd = wd.findElement(By.id("startEnd"));
        startEnd.sendKeys("03062021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Không có dữ liệu phù hợp");
    }
    @Test
    public void testDSTheoThoiGianKhongChonKhuVuc() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dstheothoigian");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
//        ddlKhuVuc.selectByIndex(2);
        Thread.sleep(1000);
        WebElement startAt = wd.findElement(By.id("startAt"));
        startAt.sendKeys("03042021");
        Thread.sleep(1000);
        WebElement startEnd = wd.findElement(By.id("startEnd"));
        startEnd.sendKeys("03062021");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Tìm kiếm thành công");
    }
    @Test
    public void testDSTheoThoiGianKhongChonNgayDen() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dstheothoigian");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(2);
        Thread.sleep(1000);
        WebElement startAt = wd.findElement(By.id("startAt"));
        startAt.sendKeys("03042021");
        Thread.sleep(1000);
//        WebElement startEnd = wd.findElement(By.id("startEnd"));
//        startEnd.sendKeys("20210603");
//        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn chưa chọn ngày kết thúc");
    }
    @Test
    public void testDSTheoThoiGianKhongChonNgayKetThuc() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dstheothoigian");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(2);
        Thread.sleep(1000);
        WebElement startAt = wd.findElement(By.id("startAt"));
        startAt.sendKeys("03042021");
        Thread.sleep(1000);
//        WebElement startEnd = wd.findElement(By.id("startEnd"));
//        startEnd.sendKeys("03062021");
//        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn chưa chọn ngày kết thúc");
    }
    @Test
    public void testDSTheoThoiGianNgayKhongHopLe() throws InterruptedException {
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/dstheothoigian");
        Select ddlKhuVuc = new Select(wd.findElement(By.id("ddlKhuVuc")));
        ddlKhuVuc.selectByIndex(2);
        Thread.sleep(1000);
        WebElement startAt = wd.findElement(By.id("startAt"));
        startAt.sendKeys("03042021");
        Thread.sleep(1000);
        WebElement startEnd = wd.findElement(By.id("startEnd"));
        startEnd.sendKeys("03042020");
        Thread.sleep(1000);
        WebElement btnSearch = wd.findElement(By.id("btnSearch"));
        btnSearch.click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Ngày không hợp lệ! Ngày bắt đầu phải nhỏ hơn ngày kết thúc");
    }
}
