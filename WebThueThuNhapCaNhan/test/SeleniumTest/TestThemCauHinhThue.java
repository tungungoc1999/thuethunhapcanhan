/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SeleniumTest;

import dao.CauHinhThueDAO;
import static dao.CauHinhThueDAOTest.rb;
import dao.RollBackDB;
import entity.CauHinhThue;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author Tran Tu
 */
//6
public class TestThemCauHinhThue {

    private static WebDriver wd = null;
    public static RollBackDB rb = new RollBackDB();

    public TestThemCauHinhThue() {

    }

    @BeforeClass
    public static void setUpClass() {
        System.setProperty("webdriver.chrome.driver", "D:/Tu/Netbean/Git_BTL_TTNCN/selenium/chromedriver.exe");
        wd = new ChromeDriver();
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/cauhinhthue");
    }

    @AfterClass
    public static void tearDownClass() {
        rb.rollBackCauHinhThue();
    }

    @Before
    public void setUp() {
        wd.navigate().refresh();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testThemCauHinhThue() throws InterruptedException {
        WebElement x = wd.findElement(By.id("btnThemMoiCau"));
        x.click();
        Thread.sleep(1000);
        WebElement bacthue = wd.findElement(By.id("bacthue"));
        WebElement tntt = wd.findElement(By.id("tntt"));
        WebElement thuexuat = wd.findElement(By.id("thuexuat"));
        WebElement cachtinh1 = wd.findElement(By.id("cachtinh1"));
        WebElement cachtinh2 = wd.findElement(By.id("cachtinh2"));
        bacthue.sendKeys("8");
        Thread.sleep(1000);
        tntt.sendKeys("90 triệu đến 100 triệu đồng");
        Thread.sleep(1000);
        thuexuat.sendKeys("40");
        Thread.sleep(1000);
        cachtinh1.sendKeys("20 triệu + 40$ TNTT trên 90 triệu");
        Thread.sleep(1000);
        cachtinh2.sendKeys("40% TNTT - 12 triệu");
        Thread.sleep(1000);
        WebElement btn = wd.findElement(By.id("btnThem"));
        Thread.sleep(1000);
        btn.click();
        Thread.sleep(2000);
    }

    @Test
    public void testThemCauHinhThue1() throws InterruptedException {
        WebElement x = wd.findElement(By.id("btnThemMoiCau"));
        x.click();
        Thread.sleep(1000);
        WebElement bacthue = wd.findElement(By.id("bacthue"));
        WebElement tntt = wd.findElement(By.id("tntt"));
        WebElement thuexuat = wd.findElement(By.id("thuexuat"));
        WebElement cachtinh1 = wd.findElement(By.id("cachtinh1"));
        WebElement cachtinh2 = wd.findElement(By.id("cachtinh2"));
        bacthue.sendKeys("");
        tntt.sendKeys("90 triệu đến 100 triệu đồng");
        Thread.sleep(1000);
        thuexuat.sendKeys("40");
        Thread.sleep(1000);
        cachtinh1.sendKeys("20 triệu + 40$ TNTT trên 90 triệu");
        Thread.sleep(1000);
        cachtinh2.sendKeys("40% TNTT - 12 triệu");
        Thread.sleep(1000);
        WebElement btn = wd.findElement(By.id("btnThem"));
        Thread.sleep(1000);
        btn.click();
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn chưa nhập bậc thuế");
    }
     @Test
    public void testThemCauHinhThue2() throws InterruptedException {
        WebElement x = wd.findElement(By.id("btnThemMoiCau"));
        x.click();
        Thread.sleep(1000);
        WebElement bacthue = wd.findElement(By.id("bacthue"));
        WebElement tntt = wd.findElement(By.id("tntt"));
        WebElement thuexuat = wd.findElement(By.id("thuexuat"));
        WebElement cachtinh1 = wd.findElement(By.id("cachtinh1"));
        WebElement cachtinh2 = wd.findElement(By.id("cachtinh2"));
        bacthue.sendKeys("8");
        Thread.sleep(1000);
        tntt.sendKeys("");
        Thread.sleep(1000);
        thuexuat.sendKeys("40");
        Thread.sleep(1000);
        cachtinh1.sendKeys("20 triệu + 40$ TNTT trên 90 triệu");
        Thread.sleep(1000);
        cachtinh2.sendKeys("40% TNTT - 12 triệu");
        Thread.sleep(1000);
        WebElement btn = wd.findElement(By.id("btnThem"));
        Thread.sleep(1000);
        btn.click();
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn chưa nhập thu nhập tính thuế");
    }
     @Test
    public void testThemCauHinhThue3() throws InterruptedException {
        WebElement x = wd.findElement(By.id("btnThemMoiCau"));
        x.click();
        Thread.sleep(1000);
        WebElement bacthue = wd.findElement(By.id("bacthue"));
        WebElement tntt = wd.findElement(By.id("tntt"));
        WebElement thuexuat = wd.findElement(By.id("thuexuat"));
        WebElement cachtinh1 = wd.findElement(By.id("cachtinh1"));
        WebElement cachtinh2 = wd.findElement(By.id("cachtinh2"));
        bacthue.sendKeys("8");
        Thread.sleep(1000);
        tntt.sendKeys("90 triệu đến 100 triệu đồng");
        Thread.sleep(1000);
        thuexuat.sendKeys("");
        Thread.sleep(1000);
        cachtinh1.sendKeys("20 triệu + 40$ TNTT trên 90 triệu");
        Thread.sleep(1000);
        cachtinh2.sendKeys("40% TNTT - 12 triệu");
        Thread.sleep(1000);
        WebElement btn = wd.findElement(By.id("btnThem"));
        Thread.sleep(1000);
        btn.click();
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn chưa nhập thuế xuất");
    }
     @Test
    public void testThemCauHinhThue4() throws InterruptedException {
        WebElement x = wd.findElement(By.id("btnThemMoiCau"));
        x.click();
        Thread.sleep(1000);
        WebElement bacthue = wd.findElement(By.id("bacthue"));
        WebElement tntt = wd.findElement(By.id("tntt"));
        WebElement thuexuat = wd.findElement(By.id("thuexuat"));
        WebElement cachtinh1 = wd.findElement(By.id("cachtinh1"));
        WebElement cachtinh2 = wd.findElement(By.id("cachtinh2"));
        bacthue.sendKeys("8");
        Thread.sleep(1000);
        tntt.sendKeys("90 triệu đến 100 triệu đồng");
        Thread.sleep(1000);
        thuexuat.sendKeys("40");
        Thread.sleep(1000);
        cachtinh1.sendKeys("");
        Thread.sleep(1000);
        cachtinh2.sendKeys("40% TNTT - 12 triệu");
        Thread.sleep(1000);
        WebElement btn = wd.findElement(By.id("btnThem"));
        Thread.sleep(1000);
        btn.click();
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn chưa nhập cách tính 1");
    }
     @Test
    public void testThemCauHinhThue5() throws InterruptedException {
        WebElement x = wd.findElement(By.id("btnThemMoiCau"));
        x.click();
        Thread.sleep(1000);
        WebElement bacthue = wd.findElement(By.id("bacthue"));
        WebElement tntt = wd.findElement(By.id("tntt"));
        WebElement thuexuat = wd.findElement(By.id("thuexuat"));
        WebElement cachtinh1 = wd.findElement(By.id("cachtinh1"));
        WebElement cachtinh2 = wd.findElement(By.id("cachtinh2"));
        bacthue.sendKeys("8");
        Thread.sleep(1000);
        tntt.sendKeys("90 triệu đến 100 triệu đồng");
        Thread.sleep(1000);
        thuexuat.sendKeys("40");
        Thread.sleep(1000);
        cachtinh1.sendKeys("20 triệu + 40$ TNTT trên 90 triệu");
        Thread.sleep(1000);
        cachtinh2.sendKeys("");
        Thread.sleep(1000);
        WebElement btn = wd.findElement(By.id("btnThem"));
        Thread.sleep(1000);
        btn.click();
        Thread.sleep(2000);
        WebElement mess = wd.findElement(By.className("toast-message"));
        assertEquals(mess.getText(), "Bạn chưa nhập cách tính 2");
    }
}
