/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SeleniumTest;

import dao.RollBackDB;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author Tran Tu
 */
//1
public class TestXoaCauHinhThue {
     private static WebDriver wd = null;
    public static RollBackDB rb = new RollBackDB();

    public TestXoaCauHinhThue() {

    }

    @BeforeClass
    public static void setUpClass() {
        System.setProperty("webdriver.chrome.driver", "D:/Tu/Netbean/Git_BTL_TTNCN/selenium/chromedriver.exe");
        wd = new ChromeDriver();
        wd.get("http://localhost:50684/WebThueThuNhapCaNhan/cauhinhthue");
    }

    @AfterClass
    public static void tearDownClass() {
        rb.rollBackCauHinhThue();
    }

    @Before
    public void setUp() {
        wd.navigate().refresh();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testXoaCauHinh() throws InterruptedException {
        WebElement x = wd.findElement(By.id("btnXoaCauHinh"));
        x.click();
        Thread.sleep(5000);
        WebElement xoa = wd.findElement(By.id("btnXoa"));
        xoa.click();
        Thread.sleep(2000);
    }
}
