/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.CauHinhThue;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tran Tu
 */
public class CauHinhThueDAOTest {
    public static RollBackDB rb = new RollBackDB();
    public CauHinhThueDAOTest() {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
        rb.rollBackCauHinhThue();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        rb.rollBackCauHinhThue();
    }

    @Test
    public void testGetAllCauHinhThueRong() {
        System.out.println("getAllCauHinhThue");
        CauHinhThueDAO instance = new CauHinhThueDAO();
        int expResult = 0;
        List<CauHinhThue> result = instance.getAllCauHinhThue();
        assertEquals(expResult, result.size());
    }
    @Test
    public void testGetAllCauHinhThue() {
        System.out.println("getAllCauHinhThue");
        CauHinhThueDAO instance = new CauHinhThueDAO();
        int expResult = 7;
        List<CauHinhThue> result = instance.getAllCauHinhThue();
        assertEquals(expResult, result.size());
    }

    @Test
    public void testFindById() {
        System.out.println("findById");
        String id = "1";
        CauHinhThueDAO instance = new CauHinhThueDAO();
        CauHinhThue expResult = new CauHinhThue(1,1, "Đến 5 triệu đồng", 5, "0 triệu đồng + 5% TNTT", "5% TNTT");
        CauHinhThue result = instance.findById(id);
        assertEquals(expResult, result);
    }
    @Test
    public void testFindByIdFail() {
        System.out.println("findById");
        String id = "8";
        CauHinhThueDAO instance = new CauHinhThueDAO();
        CauHinhThue expResult = null;
        CauHinhThue result = instance.findById(id);
        assertEquals(expResult, result);
    }

    @Test
    public void testFindLast() {
        System.out.println("findLast");
        CauHinhThueDAO instance = new CauHinhThueDAO();
        CauHinhThue expResult = new CauHinhThue(7, 7, "Trên 80 trđ", 35, "18,15 trđ + 35% TNTT trên 80 trđ", "35% TNTT - 9,85 trđ");
        CauHinhThue result = instance.findLast();
        assertEquals(expResult, result);
    }

    @Test
    public void testCreateCauHinhThue() {
        System.out.println("createCauHinhThue");
        String bacthue = "8";
        String tntt = "Trên 100tr";
        String thuexuat = "40";
        String cachtinh1 = "18,15 trđ + 35% TNTT trên 100 trđ";
        String cachtinh2 = "40% TNTT - 9,85 trđ";
        CauHinhThueDAO instance = new CauHinhThueDAO();
        boolean expResult = true;
        boolean result = instance.createCauHinhThue(bacthue, tntt, thuexuat, cachtinh1, cachtinh2);
        CauHinhThue resultC = instance.findLast();
        CauHinhThue expResultC = new CauHinhThue(resultC.getID(), 8, tntt, 40, cachtinh1, cachtinh2);
        assertTrue(expResult == result && resultC.equals(expResultC));
    }

    @Test
    public void testUpdateCauHinhThue() {
        System.out.println("updateCauHinhThue");
        String ID = "2";
        String bacthue = "50";
        String tntt = "Trên 500tr";
        String thuexuat = "50";
        String cachtinh1 = "18,15 trđ + 35% TNTT trên 100 trđ";
        String cachtinh2 = "40% TNTT - 9,85 trđ";
        CauHinhThueDAO instance = new CauHinhThueDAO();
        boolean expResult = true;
        boolean result = instance.updateCauHinhThue(ID, bacthue, tntt, thuexuat, cachtinh1, cachtinh2);
        CauHinhThue resultC = instance.findById("2");
        CauHinhThue expResultC = new CauHinhThue(2, 50, tntt, 50, cachtinh1, cachtinh2);
        assertTrue(result == true && resultC.equals(expResultC));
    }
    @Test
    public void testUpdateCauHinhThueFail() {
        System.out.println("updateCauHinhThueFail");
        String ID = "10";
        String bacthue = "50";
        String tntt = "Trên 500tr";
        String thuexuat = "50";
        String cachtinh1 = "18,15 trđ + 35% TNTT trên 100 trđ";
        String cachtinh2 = "40% TNTT - 9,85 trđ";
        CauHinhThueDAO instance = new CauHinhThueDAO();
        CauHinhThue resultC = instance.findById("2");
        boolean expResult = false;
        boolean result = instance.updateCauHinhThue(ID, bacthue, tntt, thuexuat, cachtinh1, cachtinh2);
        CauHinhThue expResultC = instance.findById("2");;
        assertTrue(expResult == result && resultC.equals(expResultC));
    }


    @Test
    public void testDeleteCauHinhThue() {
        System.out.println("deleteCauHinhThue");
        String ID = "3";
        CauHinhThueDAO instance = new CauHinhThueDAO();
        boolean expResult = true;
        boolean result = instance.deleteCauHinhThue(ID);
        CauHinhThue resultC = instance.findById("3");
         assertTrue(expResult == result && resultC == null);
    }
    
    @Test
    public void testDeleteCauHinhThueFail() {
        System.out.println("deleteCauHinhThueFail");
        String ID = "10";
        CauHinhThueDAO instance = new CauHinhThueDAO();
        boolean expResult = false;
        boolean result = instance.deleteCauHinhThue(ID);
        assertEquals(expResult, result);
    }
    
}
