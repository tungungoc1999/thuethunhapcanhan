<%-- 
    Document   : Cart
    Created on : Oct 31, 2020, 9:42:21 PM
    Author     : trinh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="gentelella/favicon.ico" type="image/ico" />
        <title>Quản lý thuế</title>
        <link href="gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="gentelella/vendors/nprogress/nprogress.css" rel="stylesheet">
        <link href="gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <link href="gentelella/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <link href="gentelella/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <link href="gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="gentelella/build/css/custom.min.css" rel="stylesheet">
        <script src="gentelella/vendors/jquery/dist/jquery.min.js"></script>
        <script src="gentelella/vendors/jquery/dist/jquery.js"></script>
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="header container-fluid" style="background:#1d446b none repeat scroll 0 0">
                    <div class="row">
                        <div class="col-md-6 col-xs-9" style="padding-top: 5px;">
                            <a href="#" class="logo" style="display:flex">
                                <img src="gentelella/logo.jpg" class="logo_top" style="height:40px" alt="Hệ thống hỗ trợ tính thuế thu nhập cá nhân">
                                <div style="    display: grid;    padding: 5px 0 5px 5px;    align-items: center; color:white">
                                    <span class="appname">Hệ thống hỗ trợ tính thuế thu nhập cá nhân</span>
                                    <span>Quản lý thuế</span>
                                </div>

                            </a>
                        </div>
                        <div class="col-md-6 col-xs-3 text-right accpanel">
                            <div class="top_nav">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view" style="width: 100%">
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <ul class="nav side-menu">
                                    <li><a href=""><i class="fa fa-home"></i> Trang chủ <span class="fa"></span></a></li>
                                    <li class="">
                                        <a ><i class="fa fa-book"></i> Xem danh sách<span class="fa"></span></a>
                                        <ul class="nav child_menu">
                                            <li style="background: none"><a href="dsdadong">Danh sách đã đóng thuế</a></li>
                                            <li style="background:none"><a href="dschuadong">Danh sách chưa đóng thuế</a></li>
                                            <li style="background:none"><a href="dstheokhuvuc">Danh sách theo khu vực</a></li>
                                            <li style="background: none"><a href="dstheothoigian">Danh sách tìm kiếm theo thời gian</a></li>
                                        </ul>
                                    </li>
                                    <!--                                    <li><a href="ExportExcel"><i class="fa fa-download"></i> Xuất excel <span class="fa"></span></a></li>-->
                                    <li class="active"><a href="cauhinhthue"><i class="fa fa-refresh"></i> Cấu hình thuế<span class="fa"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="right_col" role="main" style="position:relative">
                    <div class="nav toggle menu_btn">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <div style="margin-top:25px">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Cấu hình thuế</h2>
                                        <div class="pull-right">
                                            <button id="btnThemMoiCau" type="button" onclick="openadd()" class="btn btn-success"><i class="fa fa-plus" title="thêm mới"></i> Thêm mới</button>
                                        </div>
                                        <div class="clearfix"></div>

                                    </div>
                                    <div class="x_title">
                                        <div class="container">
                                            <div class="row">
                                            </div>
                                            <br />
                                            <hr />
                                            <div class="x_content">
                                                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                    <div>
                                                        <table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
                                                            <thead>
                                                                <tr role="row">
                                                                    <th style="text-align:center">STT</th>
                                                                    <th style="text-align:center">Bậc thuế</th>
                                                                    <th style="text-align:center">Thu nhập tính thuế ( /tháng)</th>
                                                                    <th style="text-align:center">Thuế xuất</th>
                                                                    <th style="text-align:center">Cách tính thuế 1</th>
                                                                    <th style="text-align:center">Cách tính thuế 2</th>
                                                                    <th style="text-align:center">Thao tác</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <%!int i = 1;%>
                                                                <c:forEach items="${lstCau}" var="x">
                                                                    <tr>
                                                                        <td>
                                                                            <%= i++%>
                                                                        </td>
                                                                        <td>${x.getBacThue()}</td>
                                                                        <td>${x.getThuNhapTinhThue()}</td>
                                                                        <td>${x.getThueXuat()}</td>
                                                                        <td>${x.getCachTinhThue1()}</td>
                                                                        <td>${x.getCachTinhThue2()}</td>
                                                                        <td>
                                                                            <button id="btnSuaCauHinh" type="button" onclick="openedit('${x.getID()}', '${x.getBacThue()}', '${x.getThuNhapTinhThue()}', '${x.getThueXuat()}', '${x.getCachTinhThue1()}', '${x.getCachTinhThue2()}')" class="btn btn-primary"><i class="fa fa-edit" title="sửa"></i></button>
                                                                            <button id="btnXoaCauHinh" type="button" onclick="confirmremove('${x.getID()}')" class="btn btn-danger"><i class="fa fa-trash" title="xóa"></i></button>
                                                                        </td>
                                                                    </tr>
                                                                </c:forEach>     
                                                            <div hidden><%= i = 1%></div>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="right_col" role="main">

                </div>
            </div>
        </div>

        <div class="modal fade modeladd" id="modeladd" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Thêm mới</h4>
                    </div>
                    <div class="modal-body">
                        <div class="x_content">
                            <br />
                            <form class="form-horizontal form-label-left" action="themmoi" method="get">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bacthue">
                                        Bậc thuế <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" id="bacthue" name="bacthue" required class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tntt">
                                        Thu nhập tính thuế ( /tháng)<span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" id="tntt" name="tntt" required class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="thuexuat">
                                        Thuế xuất<span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" id="thuexuat" name="thuexuat" required class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cachtinh1">
                                        Cách tính thuế 1<span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" id="cachtinh1" name="cachtinh1" required class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cachtinh2">
                                        Cách tính thuế 2<span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" id="cachtinh2" name="cachtinh2" required class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group" style="text-align:center">
                                    <button type="button" class="btn btn-primar" data-dismiss="modal">Huỷ</button>
                                    <button type="submit" id="btnThem" class="btn btn-success">Thêm</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align:center">

                    </div>

                </div>
            </div>
        </div>


        <div class="modal fade modeledit" id="modeledit" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Cập nhật</h4>
                    </div>
                    <div class="modal-body">
                        <div class="x_content">
                            <br />
                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="capnhat" method="post">
                                <input hidden id="IdHd" name="IdHd"/>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bacthue">
                                        Bậc thuế (Chỉ điền số) <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" id="bacthue_edit" name="bacthue_edit" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tntt">
                                        Thu nhập tính thuế ( /tháng)<span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" id="tntt_edit" name="tntt_edit" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="thuexuat">
                                        Thuế xuất (Chỉ điền số từ 1 đến 100)<span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" id="thuexuat_edit" name="thuexuat_edit" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cachtinh1">
                                        Cách tính thuế 1<span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" id="cachtinh1_edit" name="cachtinh1_edit" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cachtinh2">
                                        Cách tính thuế 2<span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" id="cachtinh2_edit" name="cachtinh2_edit" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group" style="text-align:center">
                                    <button type="button" class="btn btn-primar" data-dismiss="modal">Hủy</button>
                                    <button type="submit" id="btnCapNhat" class="btn btn-success">Cập nhật</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align:center">

                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade modeldelete" id="modeldelete" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Xác nhận</h4>
                    </div>
                    <form method="post" action="xoa">
                        <div class="modal-body">
                            <div class="form-group">
                                <center>
                                    <input type="hidden" name="IdXoa" id="IdXoa" value="" />
                                    <h3>Bạn có chắc muốn xóa??</h3>
                                </center>
                            </div>
                        </div>
                        <div class="modal-footer" style="text-align:center">
                            <button type="button" class="btn btn-primar" data-dismiss="modal">Hủy</button>
                            <button type="submit" id="btnXoa" class="btn btn-success">Xóa</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        <script src="gentelella/vendors/jquery/dist/jquery.min.js"></script>
        <script src="gentelella/vendors/jquery/dist/jquery.js"></script>
        <script src="gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="gentelella/vendors/fastclick/lib/fastclick.js"></script>
        <script src="gentelella/vendors/nprogress/nprogress.js"></script>
        <script src="gentelella/vendors/Chart.js/dist/Chart.min.js"></script>
        <script src="gentelella/vendors/gauge.js/dist/gauge.min.js"></script>
        <script src="gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <script src="gentelella/vendors/iCheck/icheck.min.js"></script>
        <script src="gentelella/vendors/skycons/skycons.js"></script>
        <script src="gentelella/vendors/Flot/jquery.flot.js"></script>
        <script src="gentelella/vendors/Flot/jquery.flot.pie.js"></script>
        <script src="gentelella/vendors/Flot/jquery.flot.time.js"></script>
        <script src="gentelella/vendors/Flot/jquery.flot.stack.js"></script>
        <script src="gentelella/vendors/Flot/jquery.flot.resize.js"></script>
        <script src="gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
        <script src="gentelella/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
        <script src="gentelella/vendors/flot.curvedlines/curvedLines.js"></script>
        <script src="gentelella/vendors/DateJS/build/date.js"></script>
        <script src="gentelella/vendors/jqvmap/dist/jquery.vmap.js"></script>
        <script src="gentelella/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="gentelella/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
        <link href="gentelella/production/js/toastr.css" rel="stylesheet">
        <script src="gentelella/production/js/toastr.js"></script>
        <script src="gentelella/build/js/custom.min.js"></script>




    </body>
</html>
<script>
    $(function () {
        $("#bacthue").inputFilter(function (value) {
            return /^\d*$/.test(value);
        });
        $("#thuexuat").inputFilter(function (value) {
            return /^\d*$/.test(value) && (value === "" || parseInt(value) < 101);
        });
        $("#bacthue_edit").inputFilter(function (value) {
            return /^\d*$/.test(value);
        });
        $("#thuexuat_edit").inputFilter(function (value) {
            return /^\d*$/.test(value) && (value === "" || parseInt(value) < 101);
        });
    });
    (function ($) {
        $.fn.inputFilter = function (inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        };
    }(jQuery));
    $('#btnThem').click(function () {
        var bacthue = $('#bacthue').val();
        var tntt = $('#tntt').val();
        var thuexuat = $('#thuexuat').val();
        var cachtinh1 = $('#cachtinh1').val();
        var cachtinh2 = $('#cachtinh2').val();
        if (bacthue == "") {
            toastr.error('Bạn chưa nhập bậc thuế');
            return false;
        }
        if (tntt == "") {
            toastr.error('Bạn chưa nhập thu nhập tính thuế');
            return false;
        }
        if (thuexuat == "") {
            toastr.error('Bạn chưa nhập thuế xuất');
            return false;
        }
        if (cachtinh1 == "") {
            toastr.error('Bạn chưa nhập cách tính 1');
            return false;
        }
        if (cachtinh2 == "") {
            toastr.error('Bạn chưa nhập cách tính 2');
            return false;
        }
        toastr.success('Thêm thành công');

    })
    
    $('#btnCapNhat').click(function () {
        var bacthue = $('#bacthue_edit').val();
        var tntt = $('#tntt_edit').val();
        var thuexuat = $('#thuexuat_edit').val();
        var cachtinh1 = $('#cachtinh1_edit').val();
        var cachtinh2 = $('#cachtinh2_edit').val();
        if (bacthue == "") {
            toastr.error('Bạn chưa nhập bậc thuế');
            return false;
        }
        if (tntt == "") {
            toastr.error('Bạn chưa nhập thu nhập tính thuế');
            return false;
        }
        if (thuexuat == "") {
            toastr.error('Bạn chưa nhập thuế xuất');
            return false;
        }
        if (cachtinh1 == "") {
            toastr.error('Bạn chưa nhập cách tính 1');
            return false;
        }
        if (cachtinh2 == "") {
            toastr.error('Bạn chưa nhập cách tính 2');
            return false;
        }
        toastr.success('Thêm thành công');

    })

    function openadd() {
        $('#bacthue').val("");
        $('#tntt').val("");
        $('#thuexuat').val("");
        $('#cachtinh1').val("");
        $('#cachtinh2').val("");
        $('#modeladd').modal({backdrop: 'static', keyboard: false})
    }
    ;

    function openedit(id, bacthue, tntt, thuexuat, cachtinh1, cachtinh2) {
        $('#IdHd').val(id);
        $('#bacthue_edit').val(bacthue);
        $('#tntt_edit').val(tntt);
        $('#thuexuat_edit').val(thuexuat);
        $('#cachtinh1_edit').val(cachtinh1);
        $('#cachtinh2_edit').val(cachtinh2);
        $('#modeledit').modal({backdrop: 'static', keyboard: false})
    }
    ;
    function confirmremove(id) {
        $('#IdXoa').val(id);
        $('#modeldelete').modal({backdrop: 'static', keyboard: false})
    }


</script>
