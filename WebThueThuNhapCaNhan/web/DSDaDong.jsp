<%-- 
    Document   : Cart
    Created on : Oct 31, 2020, 9:42:21 PM
    Author     : trinh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="gentelella/favicon.ico" type="image/ico" />
        <title>Quản lý thuế</title>
        <link href="gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="gentelella/vendors/nprogress/nprogress.css" rel="stylesheet">
        <link href="gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <link href="gentelella/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <link href="gentelella/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <link href="gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="gentelella/build/css/custom.min.css" rel="stylesheet">
        <script src="gentelella/vendors/jquery/dist/jquery.min.js"></script>
        <script src="gentelella/vendors/jquery/dist/jquery.js"></script>
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="header container-fluid" style="background:#1d446b none repeat scroll 0 0">
                    <div class="row">
                        <div class="col-md-6 col-xs-9" style="padding-top: 5px;">
                            <a href="#" class="logo" style="display:flex">
                                <img src="gentelella/logo.jpg" class="logo_top" style="height:40px" alt="Hệ thống hỗ trợ tính thuế thu nhập cá nhân">
                                <div style="    display: grid;    padding: 5px 0 5px 5px;    align-items: center; color:white">
                                    <span class="appname">Hệ thống hỗ trợ tính thuế thu nhập cá nhân</span>
                                    <span>Quản lý thuế</span>
                                </div>

                            </a>
                        </div>
                        <div class="col-md-6 col-xs-3 text-right accpanel">
                            <div class="top_nav">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view" style="width: 100%">
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <ul class="nav side-menu">
                                    <li><a href=""><i class="fa fa-home"></i> Trang chủ <span class="fa"></span></a></li>
                                    <li class="active">
                                        <a ><i class="fa fa-book"></i> Xem danh sách<span class="fa"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="dsdadong">Danh sách đã đóng thuế</a></li>
                                            <li style="background:none"><a href="dschuadong">Danh sách chưa đóng thuế</a></li>
                                            <li style="background: none"><a href="dstheokhuvuc">Danh sách theo khu vực</a></li>
                                            <li style="background: none"><a href="dstheothoigian">Danh sách tìm kiếm theo thời gian</a></li>
                                        </ul>
                                    </li>
                                    <!--                                    <li><a href="ExportExcel"><i class="fa fa-download"></i> Xuất excel <span class="fa"></span></a></li>-->
                                    <li ><a href="cauhinhthue"><i class="fa fa-refresh"></i> Cấu hình thuế<span class="fa"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="right_col" role="main" style="position:relative">
                    <div class="nav toggle menu_btn">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <div style="margin-top:25px">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Danh sách thu thuế cá nhân đã đóng</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-2" style="margin-left: 5px">
                                                    <label class="control-label">
                                                        Khu vực *
                                                    </label>
                                                    <select class="form-control" name="ddlKhuVuc" id="ddlKhuVuc">
                                                        <option value="0">------- Chọn -------</option>
                                                        <c:forEach items="${lstTP}" var="x">
                                                            <option value="${x.getMaTP()}">${x.getTenTinh()}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <label class="control-label">
                                                        Tháng đóng *
                                                    </label>
                                                    <select class="form-control" name="ddlThang" id="ddlThang">
                                                        <option value="0">------- Chọn -------</option>
                                                        <% for (int i = 1; i < 13; i++) {%>
                                                        <option value="<%= i%>">Tháng <%= i%></option>
                                                        <% } %>
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <label class="control-label">
                                                        Năm đóng *
                                                    </label>
                                                    <select class="form-control" name="ddlNam" id="ddlNam">
                                                        <option value="0">------- Chọn -------</option>
                                                        <% for (int i = 2015; i < 2030; i++) {%>
                                                        <option value="<%= i%>">Năm <%= i%></option>
                                                        <% }%>
                                                    </select>
                                                </div>
                                                <div class="col-md-2" style="padding-top:24px;">
                                                    <button id="btnSearch" class="btn btn-info"><i class="fa fa-search" title="tìm kiếm"></i></button>
                                                </div>
                                                <form method="post" action="ExportExcel">
                                                    <div class="col-md-3" style="padding-top:24px;">
                                                        <button disabled="" id="xuatExcel" type="submit" type="button" class="btn btn-success"><i class="fa fa-download" title="Xuất Excel"></i>Xuất Excel</button>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                        <br />
                                        <div>
                                            <div class="x_content">
                                                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                    <table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
                                                        <thead>
                                                            <tr role="row">
                                                                <th style="text-align:center">STT</th>
                                                                <th style="text-align:center">Số thuế</th>
                                                                <th style="text-align:center">Họ tên</th>
                                                                <th style="text-align:center">Số CMND/CCCD</th>
                                                                <th style="text-align:center">Thông tin</th>
                                                                <th style="text-align:center">Địa chỉ</th>
                                                                <th style="text-align:center">Thời gian đóng thuế</th>
                                                                <th style="text-align:center">Thu nhập tính thuế</th>
                                                                <th style="text-align:center">Số tiền đóng thuế</th>
                                                                <th style="text-align:center">Bậc thuế</th>
                                                                <th style="text-align:center">Tháng đóng</th>
                                                                <th style="text-align:center">Năm đóng</th>
                                                                <th style="text-align:center">Trạng thái</th>
                                                                <th style="text-align:center">Khu vực</th>
                                                            </tr>
                                                        </thead>
                                                        <%!int i = 1;%>
                                                        <tbody id="loadDuLieu">
                                                            <c:forEach items="${lstDS123}" var="x">
                                                                <tr>
                                                                    <td>
                                                                        <%= i++%>
                                                                    </td>
                                                                    <td>${x.getSoThue()}</td>
                                                                    <td>${x.getHoTen()}</td>
                                                                    <td>${x.getSoCMND()}</td>
                                                                    <td>${x.getThongTin()}</td>
                                                                    <td>${x.getDiaChi()}</td>
                                                                    <td>${x.getThoiGianDongThue()}</td>
                                                                    <td style="text-align: right">${x.getThuNhapTinhThue()}</td>
                                                                    <td style="text-align: right">${x.getSoTienDongThue()}</td>
                                                                    <td>${x.getBacThue()}</td>
                                                                    <td>${x.getThangDong()}</td>
                                                                    <td>${x.getNamDong()}</td>
                                                                    <td>${x.getTenTrangThai()}</td>
                                                                    <td>${x.getTenKhuVuc()}</td>
                                                                </tr>
                                                            </c:forEach>
                                                        </tbody>
                                                        <div hidden><%= i = 1%></div>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="right_col" role="main">

                </div>
            </div>
        </div>

        <div id="divProcessing" style="position:fixed;top:0px; left:0px; height:100%;width :100%;text-align:center;
             background:#f5f5f5;
             opacity:0.5;
             z-index:999;
             position:absolute;
             left:0px;
             top :0px;
             cursor: wait; display:none">
            <img style="position: fixed;top: 45%;" src="gentelella/processing.gif" alt="" width="70" height="70" />
        </div>

        <script src="gentelella/vendors/jquery/dist/jquery.min.js"></script>
        <script src="gentelella/vendors/jquery/dist/jquery.js"></script>
        <script src="gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="gentelella/vendors/fastclick/lib/fastclick.js"></script>
        <script src="gentelella/vendors/nprogress/nprogress.js"></script>
        <script src="gentelella/vendors/Chart.js/dist/Chart.min.js"></script>
        <script src="gentelella/vendors/gauge.js/dist/gauge.min.js"></script>
        <script src="gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <script src="gentelella/vendors/iCheck/icheck.min.js"></script>
        <script src="gentelella/vendors/skycons/skycons.js"></script>
        <script src="gentelella/vendors/Flot/jquery.flot.js"></script>
        <script src="gentelella/vendors/Flot/jquery.flot.pie.js"></script>
        <script src="gentelella/vendors/Flot/jquery.flot.time.js"></script>
        <script src="gentelella/vendors/Flot/jquery.flot.stack.js"></script>
        <script src="gentelella/vendors/Flot/jquery.flot.resize.js"></script>
        <script src="gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
        <script src="gentelella/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
        <script src="gentelella/vendors/flot.curvedlines/curvedLines.js"></script>
        <script src="gentelella/vendors/DateJS/build/date.js"></script>
        <script src="gentelella/vendors/jqvmap/dist/jquery.vmap.js"></script>
        <script src="gentelella/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="gentelella/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
        <link href="gentelella/production/js/toastr.css" rel="stylesheet">
        <script src="gentelella/production/js/toastr.js"></script>
        <script src="gentelella/build/js/custom.min.js"></script>
    </body>
</html>


<script>
    $('#btnSearch').click(function () {
        var ddlKhuVuc = $('#ddlKhuVuc').val();
        var ddlThang = $('#ddlThang').val();
        var ddlNam = $('#ddlNam').val();
        if (ddlKhuVuc == 0) {
            toastr.error("Bạn chưa chọn khu vực");
            return false;
        }
        if (ddlThang == "0") {
            toastr.error("Bạn chưa chọn tháng đóng");
            return false;
        }
        if (ddlNam == "0") {
            toastr.error("Bạn chưa chọn năm");
            return false;
        }
        $("#divProcessing").show();
        $.ajax({
            type: "get",
            url: "SearchDSDaDong",
            data: {ddlKhuVuc: ddlKhuVuc, ddlThang: ddlThang, ddlNam: ddlNam, type: 1},
            success: function (data) {
                $("#divProcessing").hide();
                var row = document.getElementById("loadDuLieu");
                if (data.trim() == "") {
                    toastr.info("Không có dữ liệu phù hợp");
                } else {
                    row.innerHTML = data;
                    toastr.success("Tìm kiếm thành công");
                    var element = document.getElementById("xuatExcel");
                    element.removeAttribute("disabled");
                }

            },
            error: function (xhr) {
                $("#divProcessing").hide();
            }
        });
    });
</script>

