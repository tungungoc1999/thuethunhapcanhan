USE [ThueThuNhapCaNhan]
GO
/****** Object:  Table [dbo].[CauHinhThue]    Script Date: 02/06/2021 22:27:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CauHinhThue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BacThue] [int] NULL,
	[ThuNhapTinhThue] [nvarchar](max) NULL,
	[ThueXuat] [int] NULL,
	[CachTinhThue1] [nvarchar](max) NULL,
	[CachTinhThue2] [nvarchar](max) NULL,
 CONSTRAINT [PK_CauHinhThue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DanhSachDongTienTheoKhuVuc]    Script Date: 02/06/2021 22:27:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DanhSachDongTienTheoKhuVuc](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[KhuVuc] [nvarchar](50) NULL,
	[TenKhuVuc] [nvarchar](50) NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[TongTienDaDong] [nvarchar](50) NULL,
 CONSTRAINT [PK_DanhSachDongTienTheoKhuVuc] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DanhSachThuThue]    Script Date: 02/06/2021 22:27:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DanhSachThuThue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SoThue] [nvarchar](50) NULL,
	[HoTen] [nvarchar](50) NULL,
	[SoCMND] [nvarchar](50) NULL,
	[ThongTin] [nvarchar](max) NULL,
	[DiaChi] [nvarchar](max) NULL,
	[ThoiGianDongThue] [date] NULL,
	[ThuNhapTinhThue] [nvarchar](50) NULL,
	[SoTienDongThue] [nvarchar](50) NULL,
	[BacThue] [int] NULL,
	[ThangDong] [int] NULL,
	[NamDong] [int] NULL,
	[UpdateBy] [nvarchar](max) NULL,
	[TrangThai] [int] NULL,
	[KhuVuc] [nvarchar](50) NULL,
	[TenTrangThai] [nvarchar](50) NULL,
	[TenKhuVuc] [nvarchar](50) NULL,
 CONSTRAINT [PK_DanhSachThuThue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tinh_Thanh]    Script Date: 02/06/2021 22:27:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tinh_Thanh](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaTP] [int] NULL,
	[TenTinh] [nvarchar](50) NULL,
	[Cap] [nvarchar](50) NULL,
 CONSTRAINT [PK_Tinh_Thanh] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CauHinhThue] ON 

INSERT [dbo].[CauHinhThue] ([ID], [BacThue], [ThuNhapTinhThue], [ThueXuat], [CachTinhThue1], [CachTinhThue2]) VALUES (1, 1, N'Đến 5 triệu đồng', 5, N'0 triệu đồng + 5% TNTT', N'5% TNTT')
INSERT [dbo].[CauHinhThue] ([ID], [BacThue], [ThuNhapTinhThue], [ThueXuat], [CachTinhThue1], [CachTinhThue2]) VALUES (2, 2, N'Trên 05 trđ đến 10 trđ', 10, N'0,25 trđ + 10% TNTT trên 5 trđ', N'10% TNTT - 0,25 trđ')
INSERT [dbo].[CauHinhThue] ([ID], [BacThue], [ThuNhapTinhThue], [ThueXuat], [CachTinhThue1], [CachTinhThue2]) VALUES (3, 3, N'Trên 10 trđ đến 18 trđ', 15, N'0,75 trđ + 15% TNTT trên 10 trđ', N'15% TNTT - 0,75 trđ')
INSERT [dbo].[CauHinhThue] ([ID], [BacThue], [ThuNhapTinhThue], [ThueXuat], [CachTinhThue1], [CachTinhThue2]) VALUES (4, 4, N'Trên 18 trđ đến 32 trđ', 20, N'1,95 trđ + 20% TNTT trên 18 trđ', N'20% TNTT - 1,65 trđ')
INSERT [dbo].[CauHinhThue] ([ID], [BacThue], [ThuNhapTinhThue], [ThueXuat], [CachTinhThue1], [CachTinhThue2]) VALUES (5, 5, N'Trên 32 trđ đến 52 trđ', 25, N'4,75 trđ + 25% TNTT trên 32 trđ', N'25% TNTT - 3,25 trđ')
INSERT [dbo].[CauHinhThue] ([ID], [BacThue], [ThuNhapTinhThue], [ThueXuat], [CachTinhThue1], [CachTinhThue2]) VALUES (6, 6, N'Trên 52 trđ đến 80 trđ', 30, N'9,75 trđ + 30% TNTT trên 52 trđ', N'30 % TNTT - 5,85 trđ')
INSERT [dbo].[CauHinhThue] ([ID], [BacThue], [ThuNhapTinhThue], [ThueXuat], [CachTinhThue1], [CachTinhThue2]) VALUES (7, 7, N'Trên 80 trđ', 35, N'18,15 trđ + 35% TNTT trên 80 trđ', N'35% TNTT - 9,85 trđ')
SET IDENTITY_INSERT [dbo].[CauHinhThue] OFF
GO
SET IDENTITY_INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ON 

INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (2, N'1', N'Thành phố Hà Nội', 5, 2021, N'20000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (3, N'2', N'Tỉnh Hà Giang', 5, 2021, N'210000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (4, N'4', N'Tỉnh Cao Bằng', 5, 2021, N'660000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (5, N'6', N'Tỉnh Bắc Kạn', 5, 2021, N'420000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (6, N'8', N'Tỉnh Tuyên Quang', 5, 2021, N'850000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (7, N'10', N'Tỉnh Lào Cai', 5, 2021, N'260000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (8, N'11', N'Tỉnh Điện Biên', 5, 2021, N'310000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (9, N'12', N'Tỉnh Lai Châu', 5, 2021, N'180000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (10, N'14', N'Tỉnh Sơn La', 5, 2021, N'900000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (11, N'15', N'Tỉnh Yên Bái', 5, 2021, N'170000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (12, N'17', N'Tỉnh Hòa Bình', 5, 2021, N'830000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (13, N'19', N'Tỉnh Thái Nguyên', 5, 2021, N'800000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (14, N'20', N'Tỉnh Lạng Sơn', 5, 2021, N'790000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (15, N'22', N'Tỉnh Quảng Ninh', 5, 2021, N'720000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (16, N'24', N'Tỉnh Bắc Giang', 5, 2021, N'720000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (17, N'25', N'Tỉnh Phú Thọ', 5, 2021, N'50000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (18, N'26', N'Tỉnh Vĩnh Phúc', 5, 2021, N'810000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (19, N'27', N'Tỉnh Bắc Ninh', 5, 2021, N'910000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (20, N'30', N'Tỉnh Hải Dương', 5, 2021, N'650000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (21, N'31', N'Thành phố Hải Phòng', 5, 2021, N'360000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (22, N'33', N'Tỉnh Hưng Yên', 5, 2021, N'520000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (23, N'34', N'Tỉnh Thái Bình', 5, 2021, N'830000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (24, N'35', N'Tỉnh Hà Nam', 5, 2021, N'260000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (25, N'36', N'Tỉnh Nam Định', 5, 2021, N'580000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (26, N'37', N'Tỉnh Ninh Bình', 5, 2021, N'380000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (27, N'38', N'Tỉnh Thanh Hóa', 5, 2021, N'450000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (28, N'40', N'Tỉnh Nghệ An', 5, 2021, N'570000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (29, N'42', N'Tỉnh Hà Tĩnh', 5, 2021, N'860000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (30, N'44', N'Tỉnh Quảng Bình', 5, 2021, N'110000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (31, N'45', N'Tỉnh Quảng Trị', 5, 2021, N'450000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (32, N'46', N'Tỉnh Thừa Thiên Huế', 5, 2021, N'450000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (33, N'48', N'Thành phố Đà Nẵng', 5, 2021, N'190000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (34, N'49', N'Tỉnh Quảng Nam', 5, 2021, N'110000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (35, N'51', N'Tỉnh Quảng Ngãi', 5, 2021, N'410000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (36, N'52', N'Tỉnh Bình Định', 5, 2021, N'990000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (37, N'54', N'Tỉnh Phú Yên', 5, 2021, N'870000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (38, N'56', N'Tỉnh Khánh Hòa', 5, 2021, N'660000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (39, N'58', N'Tỉnh Ninh Thuận', 5, 2021, N'140000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (40, N'60', N'Tỉnh Bình Thuận', 5, 2021, N'180000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (41, N'62', N'Tỉnh Kon Tum', 5, 2021, N'950000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (42, N'64', N'Tỉnh Gia Lai', 5, 2021, N'680000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (43, N'66', N'Tỉnh Đắk Lắk', 5, 2021, N'430000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (44, N'67', N'Tỉnh Đắk Nông', 5, 2021, N'160000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (45, N'68', N'Tỉnh Lâm Đồng', 5, 2021, N'770000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (46, N'70', N'Tỉnh Bình Phước', 5, 2021, N'960000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (47, N'72', N'Tỉnh Tây Ninh', 5, 2021, N'660000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (48, N'74', N'Tỉnh Bình Dương', 5, 2021, N'950000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (49, N'75', N'Tỉnh Đồng Nai', 5, 2021, N'900000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (50, N'77', N'Tỉnh Bà Rịa - Vũng Tàu', 5, 2021, N'830000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (51, N'79', N'Thành phố Hồ Chí Minh', 5, 2021, N'870000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (52, N'80', N'Tỉnh Long An', 5, 2021, N'100000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (53, N'82', N'Tỉnh Tiền Giang', 5, 2021, N'800000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (54, N'83', N'Tỉnh Bến Tre', 5, 2021, N'910000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (55, N'84', N'Tỉnh Trà Vinh', 5, 2021, N'860000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (56, N'86', N'Tỉnh Vĩnh Long', 5, 2021, N'150000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (57, N'87', N'Tỉnh Đồng Tháp', 5, 2021, N'100000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (58, N'89', N'Tỉnh An Giang', 5, 2021, N'20000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (59, N'91', N'Tỉnh Kiên Giang', 5, 2021, N'930000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (60, N'92', N'Thành phố Cần Thơ', 5, 2021, N'350000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (61, N'93', N'Tỉnh Hậu Giang', 5, 2021, N'40000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (62, N'94', N'Tỉnh Sóc Trăng', 5, 2021, N'300000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (63, N'95', N'Tỉnh Bạc Liêu', 5, 2021, N'840000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (64, N'96', N'Tỉnh Cà Mau', 5, 2021, N'900000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (65, N'1', N'Thành phố Hà Nội', 6, 2021, N'50000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (66, N'2', N'Tỉnh Hà Giang', 6, 2021, N'270000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (67, N'4', N'Tỉnh Cao Bằng', 6, 2021, N'740000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (68, N'6', N'Tỉnh Bắc Kạn', 6, 2021, N'340000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (69, N'8', N'Tỉnh Tuyên Quang', 6, 2021, N'180000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (70, N'10', N'Tỉnh Lào Cai', 6, 2021, N'70000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (71, N'11', N'Tỉnh Điện Biên', 6, 2021, N'530000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (72, N'12', N'Tỉnh Lai Châu', 6, 2021, N'920000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (73, N'14', N'Tỉnh Sơn La', 6, 2021, N'900000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (74, N'15', N'Tỉnh Yên Bái', 6, 2021, N'50000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (75, N'17', N'Tỉnh Hòa Bình', 6, 2021, N'280000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (76, N'19', N'Tỉnh Thái Nguyên', 6, 2021, N'720000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (77, N'20', N'Tỉnh Lạng Sơn', 6, 2021, N'890000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (78, N'22', N'Tỉnh Quảng Ninh', 6, 2021, N'920000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (79, N'24', N'Tỉnh Bắc Giang', 6, 2021, N'90000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (80, N'25', N'Tỉnh Phú Thọ', 6, 2021, N'160000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (81, N'26', N'Tỉnh Vĩnh Phúc', 6, 2021, N'990000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (82, N'27', N'Tỉnh Bắc Ninh', 6, 2021, N'740000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (83, N'30', N'Tỉnh Hải Dương', 6, 2021, N'680000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (84, N'31', N'Thành phố Hải Phòng', 6, 2021, N'760000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (85, N'33', N'Tỉnh Hưng Yên', 6, 2021, N'290000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (86, N'34', N'Tỉnh Thái Bình', 6, 2021, N'890000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (87, N'35', N'Tỉnh Hà Nam', 6, 2021, N'90000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (88, N'36', N'Tỉnh Nam Định', 6, 2021, N'0')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (89, N'37', N'Tỉnh Ninh Bình', 6, 2021, N'180000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (90, N'38', N'Tỉnh Thanh Hóa', 6, 2021, N'620000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (91, N'40', N'Tỉnh Nghệ An', 6, 2021, N'350000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (92, N'42', N'Tỉnh Hà Tĩnh', 6, 2021, N'200000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (93, N'44', N'Tỉnh Quảng Bình', 6, 2021, N'890000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (94, N'45', N'Tỉnh Quảng Trị', 6, 2021, N'890000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (95, N'46', N'Tỉnh Thừa Thiên Huế', 6, 2021, N'650000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (96, N'48', N'Thành phố Đà Nẵng', 6, 2021, N'550000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (97, N'49', N'Tỉnh Quảng Nam', 6, 2021, N'160000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (98, N'51', N'Tỉnh Quảng Ngãi', 6, 2021, N'10000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (99, N'52', N'Tỉnh Bình Định', 6, 2021, N'890000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (100, N'54', N'Tỉnh Phú Yên', 6, 2021, N'500000000')
GO
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (101, N'56', N'Tỉnh Khánh Hòa', 6, 2021, N'110000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (102, N'58', N'Tỉnh Ninh Thuận', 6, 2021, N'710000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (103, N'60', N'Tỉnh Bình Thuận', 6, 2021, N'740000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (104, N'62', N'Tỉnh Kon Tum', 6, 2021, N'460000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (105, N'64', N'Tỉnh Gia Lai', 6, 2021, N'750000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (106, N'66', N'Tỉnh Đắk Lắk', 6, 2021, N'550000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (107, N'67', N'Tỉnh Đắk Nông', 6, 2021, N'950000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (108, N'68', N'Tỉnh Lâm Đồng', 6, 2021, N'60000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (109, N'70', N'Tỉnh Bình Phước', 6, 2021, N'110000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (110, N'72', N'Tỉnh Tây Ninh', 6, 2021, N'790000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (111, N'74', N'Tỉnh Bình Dương', 6, 2021, N'310000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (112, N'75', N'Tỉnh Đồng Nai', 6, 2021, N'730000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (113, N'77', N'Tỉnh Bà Rịa - Vũng Tàu', 6, 2021, N'880000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (114, N'79', N'Thành phố Hồ Chí Minh', 6, 2021, N'900000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (115, N'80', N'Tỉnh Long An', 6, 2021, N'840000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (116, N'82', N'Tỉnh Tiền Giang', 6, 2021, N'670000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (117, N'83', N'Tỉnh Bến Tre', 6, 2021, N'730000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (118, N'84', N'Tỉnh Trà Vinh', 6, 2021, N'560000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (119, N'86', N'Tỉnh Vĩnh Long', 6, 2021, N'620000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (120, N'87', N'Tỉnh Đồng Tháp', 6, 2021, N'540000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (121, N'89', N'Tỉnh An Giang', 6, 2021, N'810000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (122, N'91', N'Tỉnh Kiên Giang', 6, 2021, N'790000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (123, N'92', N'Thành phố Cần Thơ', 6, 2021, N'10000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (124, N'93', N'Tỉnh Hậu Giang', 6, 2021, N'600000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (125, N'94', N'Tỉnh Sóc Trăng', 6, 2021, N'210000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (126, N'95', N'Tỉnh Bạc Liêu', 6, 2021, N'380000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (127, N'96', N'Tỉnh Cà Mau', 6, 2021, N'310000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (128, N'1', N'Thành phố Hà Nội', 4, 2021, N'150000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (129, N'2', N'Tỉnh Hà Giang', 4, 2021, N'500000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (130, N'4', N'Tỉnh Cao Bằng', 4, 2021, N'10000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (131, N'6', N'Tỉnh Bắc Kạn', 4, 2021, N'10000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (132, N'8', N'Tỉnh Tuyên Quang', 4, 2021, N'470000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (133, N'10', N'Tỉnh Lào Cai', 4, 2021, N'200000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (134, N'11', N'Tỉnh Điện Biên', 4, 2021, N'700000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (135, N'12', N'Tỉnh Lai Châu', 4, 2021, N'820000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (136, N'14', N'Tỉnh Sơn La', 4, 2021, N'730000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (137, N'15', N'Tỉnh Yên Bái', 4, 2021, N'900000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (138, N'17', N'Tỉnh Hòa Bình', 4, 2021, N'980000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (139, N'19', N'Tỉnh Thái Nguyên', 4, 2021, N'410000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (140, N'20', N'Tỉnh Lạng Sơn', 4, 2021, N'20000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (141, N'22', N'Tỉnh Quảng Ninh', 4, 2021, N'240000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (142, N'24', N'Tỉnh Bắc Giang', 4, 2021, N'670000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (143, N'25', N'Tỉnh Phú Thọ', 4, 2021, N'240000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (144, N'26', N'Tỉnh Vĩnh Phúc', 4, 2021, N'110000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (145, N'27', N'Tỉnh Bắc Ninh', 4, 2021, N'160000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (146, N'30', N'Tỉnh Hải Dương', 4, 2021, N'130000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (147, N'31', N'Thành phố Hải Phòng', 4, 2021, N'90000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (148, N'33', N'Tỉnh Hưng Yên', 4, 2021, N'290000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (149, N'34', N'Tỉnh Thái Bình', 4, 2021, N'170000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (150, N'35', N'Tỉnh Hà Nam', 4, 2021, N'140000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (151, N'36', N'Tỉnh Nam Định', 4, 2021, N'230000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (152, N'37', N'Tỉnh Ninh Bình', 4, 2021, N'560000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (153, N'38', N'Tỉnh Thanh Hóa', 4, 2021, N'470000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (154, N'40', N'Tỉnh Nghệ An', 4, 2021, N'650000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (155, N'42', N'Tỉnh Hà Tĩnh', 4, 2021, N'750000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (156, N'44', N'Tỉnh Quảng Bình', 4, 2021, N'650000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (157, N'45', N'Tỉnh Quảng Trị', 4, 2021, N'520000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (158, N'46', N'Tỉnh Thừa Thiên Huế', 4, 2021, N'490000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (159, N'48', N'Thành phố Đà Nẵng', 4, 2021, N'520000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (160, N'49', N'Tỉnh Quảng Nam', 4, 2021, N'240000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (161, N'51', N'Tỉnh Quảng Ngãi', 4, 2021, N'840000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (162, N'52', N'Tỉnh Bình Định', 4, 2021, N'460000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (163, N'54', N'Tỉnh Phú Yên', 4, 2021, N'120000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (164, N'56', N'Tỉnh Khánh Hòa', 4, 2021, N'810000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (165, N'58', N'Tỉnh Ninh Thuận', 4, 2021, N'20000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (166, N'60', N'Tỉnh Bình Thuận', 4, 2021, N'40000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (167, N'62', N'Tỉnh Kon Tum', 4, 2021, N'250000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (168, N'64', N'Tỉnh Gia Lai', 4, 2021, N'870000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (169, N'66', N'Tỉnh Đắk Lắk', 4, 2021, N'920000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (170, N'67', N'Tỉnh Đắk Nông', 4, 2021, N'930000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (171, N'68', N'Tỉnh Lâm Đồng', 4, 2021, N'360000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (172, N'70', N'Tỉnh Bình Phước', 4, 2021, N'70000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (173, N'72', N'Tỉnh Tây Ninh', 4, 2021, N'260000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (174, N'74', N'Tỉnh Bình Dương', 4, 2021, N'780000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (175, N'75', N'Tỉnh Đồng Nai', 4, 2021, N'800000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (176, N'77', N'Tỉnh Bà Rịa - Vũng Tàu', 4, 2021, N'270000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (177, N'79', N'Thành phố Hồ Chí Minh', 4, 2021, N'780000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (178, N'80', N'Tỉnh Long An', 4, 2021, N'420000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (179, N'82', N'Tỉnh Tiền Giang', 4, 2021, N'190000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (180, N'83', N'Tỉnh Bến Tre', 4, 2021, N'550000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (181, N'84', N'Tỉnh Trà Vinh', 4, 2021, N'550000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (182, N'86', N'Tỉnh Vĩnh Long', 4, 2021, N'850000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (183, N'87', N'Tỉnh Đồng Tháp', 4, 2021, N'430000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (184, N'89', N'Tỉnh An Giang', 4, 2021, N'980000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (185, N'91', N'Tỉnh Kiên Giang', 4, 2021, N'110000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (186, N'92', N'Thành phố Cần Thơ', 4, 2021, N'910000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (187, N'93', N'Tỉnh Hậu Giang', 4, 2021, N'0')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (188, N'94', N'Tỉnh Sóc Trăng', 4, 2021, N'130000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (189, N'95', N'Tỉnh Bạc Liêu', 4, 2021, N'310000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (190, N'96', N'Tỉnh Cà Mau', 4, 2021, N'810000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (191, N'1', N'Thành phố Hà Nội', 3, 2021, N'220000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (192, N'2', N'Tỉnh Hà Giang', 3, 2021, N'130000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (193, N'4', N'Tỉnh Cao Bằng', 3, 2021, N'850000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (194, N'6', N'Tỉnh Bắc Kạn', 3, 2021, N'370000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (195, N'8', N'Tỉnh Tuyên Quang', 3, 2021, N'390000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (196, N'10', N'Tỉnh Lào Cai', 3, 2021, N'910000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (197, N'11', N'Tỉnh Điện Biên', 3, 2021, N'790000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (198, N'12', N'Tỉnh Lai Châu', 3, 2021, N'480000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (199, N'14', N'Tỉnh Sơn La', 3, 2021, N'690000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (200, N'15', N'Tỉnh Yên Bái', 3, 2021, N'960000000')
GO
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (201, N'17', N'Tỉnh Hòa Bình', 3, 2021, N'410000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (202, N'19', N'Tỉnh Thái Nguyên', 3, 2021, N'700000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (203, N'20', N'Tỉnh Lạng Sơn', 3, 2021, N'230000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (204, N'22', N'Tỉnh Quảng Ninh', 3, 2021, N'150000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (205, N'24', N'Tỉnh Bắc Giang', 3, 2021, N'630000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (206, N'25', N'Tỉnh Phú Thọ', 3, 2021, N'700000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (207, N'26', N'Tỉnh Vĩnh Phúc', 3, 2021, N'10000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (208, N'27', N'Tỉnh Bắc Ninh', 3, 2021, N'420000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (209, N'30', N'Tỉnh Hải Dương', 3, 2021, N'330000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (210, N'31', N'Thành phố Hải Phòng', 3, 2021, N'420000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (211, N'33', N'Tỉnh Hưng Yên', 3, 2021, N'200000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (212, N'34', N'Tỉnh Thái Bình', 3, 2021, N'940000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (213, N'35', N'Tỉnh Hà Nam', 3, 2021, N'470000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (214, N'36', N'Tỉnh Nam Định', 3, 2021, N'820000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (215, N'37', N'Tỉnh Ninh Bình', 3, 2021, N'360000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (216, N'38', N'Tỉnh Thanh Hóa', 3, 2021, N'520000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (217, N'40', N'Tỉnh Nghệ An', 3, 2021, N'900000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (218, N'42', N'Tỉnh Hà Tĩnh', 3, 2021, N'470000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (219, N'44', N'Tỉnh Quảng Bình', 3, 2021, N'860000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (220, N'45', N'Tỉnh Quảng Trị', 3, 2021, N'410000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (221, N'46', N'Tỉnh Thừa Thiên Huế', 3, 2021, N'770000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (222, N'48', N'Thành phố Đà Nẵng', 3, 2021, N'280000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (223, N'49', N'Tỉnh Quảng Nam', 3, 2021, N'320000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (224, N'51', N'Tỉnh Quảng Ngãi', 3, 2021, N'340000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (225, N'52', N'Tỉnh Bình Định', 3, 2021, N'530000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (226, N'54', N'Tỉnh Phú Yên', 3, 2021, N'450000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (227, N'56', N'Tỉnh Khánh Hòa', 3, 2021, N'130000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (228, N'58', N'Tỉnh Ninh Thuận', 3, 2021, N'500000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (229, N'60', N'Tỉnh Bình Thuận', 3, 2021, N'510000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (230, N'62', N'Tỉnh Kon Tum', 3, 2021, N'970000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (231, N'64', N'Tỉnh Gia Lai', 3, 2021, N'90000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (232, N'66', N'Tỉnh Đắk Lắk', 3, 2021, N'80000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (233, N'67', N'Tỉnh Đắk Nông', 3, 2021, N'80000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (234, N'68', N'Tỉnh Lâm Đồng', 3, 2021, N'140000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (235, N'70', N'Tỉnh Bình Phước', 3, 2021, N'420000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (236, N'72', N'Tỉnh Tây Ninh', 3, 2021, N'90000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (237, N'74', N'Tỉnh Bình Dương', 3, 2021, N'790000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (238, N'75', N'Tỉnh Đồng Nai', 3, 2021, N'510000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (239, N'77', N'Tỉnh Bà Rịa - Vũng Tàu', 3, 2021, N'310000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (240, N'79', N'Thành phố Hồ Chí Minh', 3, 2021, N'510000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (241, N'80', N'Tỉnh Long An', 3, 2021, N'320000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (242, N'82', N'Tỉnh Tiền Giang', 3, 2021, N'60000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (243, N'83', N'Tỉnh Bến Tre', 3, 2021, N'340000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (244, N'84', N'Tỉnh Trà Vinh', 3, 2021, N'970000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (245, N'86', N'Tỉnh Vĩnh Long', 3, 2021, N'210000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (246, N'87', N'Tỉnh Đồng Tháp', 3, 2021, N'80000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (247, N'89', N'Tỉnh An Giang', 3, 2021, N'290000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (248, N'91', N'Tỉnh Kiên Giang', 3, 2021, N'360000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (249, N'92', N'Thành phố Cần Thơ', 3, 2021, N'850000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (250, N'93', N'Tỉnh Hậu Giang', 3, 2021, N'260000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (251, N'94', N'Tỉnh Sóc Trăng', 3, 2021, N'820000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (252, N'95', N'Tỉnh Bạc Liêu', 3, 2021, N'620000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (253, N'96', N'Tỉnh Cà Mau', 3, 2021, N'350000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (254, N'1', N'Thành phố Hà Nội', 2, 2021, N'370000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (255, N'2', N'Tỉnh Hà Giang', 2, 2021, N'680000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (256, N'4', N'Tỉnh Cao Bằng', 2, 2021, N'550000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (257, N'6', N'Tỉnh Bắc Kạn', 2, 2021, N'690000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (258, N'8', N'Tỉnh Tuyên Quang', 2, 2021, N'900000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (259, N'10', N'Tỉnh Lào Cai', 2, 2021, N'810000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (260, N'11', N'Tỉnh Điện Biên', 2, 2021, N'180000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (261, N'12', N'Tỉnh Lai Châu', 2, 2021, N'560000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (262, N'14', N'Tỉnh Sơn La', 2, 2021, N'590000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (263, N'15', N'Tỉnh Yên Bái', 2, 2021, N'970000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (264, N'17', N'Tỉnh Hòa Bình', 2, 2021, N'430000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (265, N'19', N'Tỉnh Thái Nguyên', 2, 2021, N'690000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (266, N'20', N'Tỉnh Lạng Sơn', 2, 2021, N'850000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (267, N'22', N'Tỉnh Quảng Ninh', 2, 2021, N'440000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (268, N'24', N'Tỉnh Bắc Giang', 2, 2021, N'640000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (269, N'25', N'Tỉnh Phú Thọ', 2, 2021, N'680000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (270, N'26', N'Tỉnh Vĩnh Phúc', 2, 2021, N'850000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (271, N'27', N'Tỉnh Bắc Ninh', 2, 2021, N'680000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (272, N'30', N'Tỉnh Hải Dương', 2, 2021, N'970000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (273, N'31', N'Thành phố Hải Phòng', 2, 2021, N'220000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (274, N'33', N'Tỉnh Hưng Yên', 2, 2021, N'240000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (275, N'34', N'Tỉnh Thái Bình', 2, 2021, N'220000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (276, N'35', N'Tỉnh Hà Nam', 2, 2021, N'90000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (277, N'36', N'Tỉnh Nam Định', 2, 2021, N'440000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (278, N'37', N'Tỉnh Ninh Bình', 2, 2021, N'930000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (279, N'38', N'Tỉnh Thanh Hóa', 2, 2021, N'780000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (280, N'40', N'Tỉnh Nghệ An', 2, 2021, N'400000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (281, N'42', N'Tỉnh Hà Tĩnh', 2, 2021, N'70000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (282, N'44', N'Tỉnh Quảng Bình', 2, 2021, N'340000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (283, N'45', N'Tỉnh Quảng Trị', 2, 2021, N'450000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (284, N'46', N'Tỉnh Thừa Thiên Huế', 2, 2021, N'210000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (285, N'48', N'Thành phố Đà Nẵng', 2, 2021, N'90000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (286, N'49', N'Tỉnh Quảng Nam', 2, 2021, N'510000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (287, N'51', N'Tỉnh Quảng Ngãi', 2, 2021, N'100000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (288, N'52', N'Tỉnh Bình Định', 2, 2021, N'810000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (289, N'54', N'Tỉnh Phú Yên', 2, 2021, N'350000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (290, N'56', N'Tỉnh Khánh Hòa', 2, 2021, N'410000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (291, N'58', N'Tỉnh Ninh Thuận', 2, 2021, N'380000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (292, N'60', N'Tỉnh Bình Thuận', 2, 2021, N'270000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (293, N'62', N'Tỉnh Kon Tum', 2, 2021, N'760000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (294, N'64', N'Tỉnh Gia Lai', 2, 2021, N'380000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (295, N'66', N'Tỉnh Đắk Lắk', 2, 2021, N'410000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (296, N'67', N'Tỉnh Đắk Nông', 2, 2021, N'550000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (297, N'68', N'Tỉnh Lâm Đồng', 2, 2021, N'490000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (298, N'70', N'Tỉnh Bình Phước', 2, 2021, N'470000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (299, N'72', N'Tỉnh Tây Ninh', 2, 2021, N'470000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (300, N'74', N'Tỉnh Bình Dương', 2, 2021, N'300000000')
GO
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (301, N'75', N'Tỉnh Đồng Nai', 2, 2021, N'220000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (302, N'77', N'Tỉnh Bà Rịa - Vũng Tàu', 2, 2021, N'430000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (303, N'79', N'Thành phố Hồ Chí Minh', 2, 2021, N'950000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (304, N'80', N'Tỉnh Long An', 2, 2021, N'790000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (305, N'82', N'Tỉnh Tiền Giang', 2, 2021, N'560000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (306, N'83', N'Tỉnh Bến Tre', 2, 2021, N'240000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (307, N'84', N'Tỉnh Trà Vinh', 2, 2021, N'630000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (308, N'86', N'Tỉnh Vĩnh Long', 2, 2021, N'720000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (309, N'87', N'Tỉnh Đồng Tháp', 2, 2021, N'610000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (310, N'89', N'Tỉnh An Giang', 2, 2021, N'290000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (311, N'91', N'Tỉnh Kiên Giang', 2, 2021, N'490000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (312, N'92', N'Thành phố Cần Thơ', 2, 2021, N'200000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (313, N'93', N'Tỉnh Hậu Giang', 2, 2021, N'900000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (314, N'94', N'Tỉnh Sóc Trăng', 2, 2021, N'250000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (315, N'95', N'Tỉnh Bạc Liêu', 2, 2021, N'390000000')
INSERT [dbo].[DanhSachDongTienTheoKhuVuc] ([Id], [KhuVuc], [TenKhuVuc], [Thang], [Nam], [TongTienDaDong]) VALUES (316, N'96', N'Tỉnh Cà Mau', 2, 2021, N'210000000')
SET IDENTITY_INSERT [dbo].[DanhSachDongTienTheoKhuVuc] OFF
GO
SET IDENTITY_INSERT [dbo].[DanhSachThuThue] ON 

INSERT [dbo].[DanhSachThuThue] ([ID], [SoThue], [HoTen], [SoCMND], [ThongTin], [DiaChi], [ThoiGianDongThue], [ThuNhapTinhThue], [SoTienDongThue], [BacThue], [ThangDong], [NamDong], [UpdateBy], [TrangThai], [KhuVuc], [TenTrangThai], [TenKhuVuc]) VALUES (1, N'1', N'Đặng Trần Tú', N'123456789', N'Sinh viên', N'Dương Nội', CAST(N'2021-05-02' AS Date), N'10000000', N'500000', 1, 5, 2021, N'', 1, N'1', N'Đã đóng', N'Thành phố Hà Nội')
INSERT [dbo].[DanhSachThuThue] ([ID], [SoThue], [HoTen], [SoCMND], [ThongTin], [DiaChi], [ThoiGianDongThue], [ThuNhapTinhThue], [SoTienDongThue], [BacThue], [ThangDong], [NamDong], [UpdateBy], [TrangThai], [KhuVuc], [TenTrangThai], [TenKhuVuc]) VALUES (2, N'1', N'Đặng Trần Tú', N'123456789', N'Sinh viên', N'Dương Nội', NULL, N'10000000', N'500000', 1, 6, 2021, N'', 2, N'1', N'Chưa đóng', N'Thành phố Hà Nội')
INSERT [dbo].[DanhSachThuThue] ([ID], [SoThue], [HoTen], [SoCMND], [ThongTin], [DiaChi], [ThoiGianDongThue], [ThuNhapTinhThue], [SoTienDongThue], [BacThue], [ThangDong], [NamDong], [UpdateBy], [TrangThai], [KhuVuc], [TenTrangThai], [TenKhuVuc]) VALUES (3, N'1', N'Đặng Trần Tú', N'123456789', N'Sinh viên', N'Dương Nội', NULL, N'10000000', N'500000', 1, 7, 2021, N'', 2, N'1', N'Chưa đóng', N'Thành phố Hà Nội')
INSERT [dbo].[DanhSachThuThue] ([ID], [SoThue], [HoTen], [SoCMND], [ThongTin], [DiaChi], [ThoiGianDongThue], [ThuNhapTinhThue], [SoTienDongThue], [BacThue], [ThangDong], [NamDong], [UpdateBy], [TrangThai], [KhuVuc], [TenTrangThai], [TenKhuVuc]) VALUES (4, N'2', N'Lưu Thế Việt', N'111111111', N'Sinh viên', N'Hà Nam', CAST(N'2021-05-02' AS Date), N'12000000', N'600000', 1, 5, 2021, N'', 1, N'35', N'Đã đóng', N'Tỉnh Hà Nam')
INSERT [dbo].[DanhSachThuThue] ([ID], [SoThue], [HoTen], [SoCMND], [ThongTin], [DiaChi], [ThoiGianDongThue], [ThuNhapTinhThue], [SoTienDongThue], [BacThue], [ThangDong], [NamDong], [UpdateBy], [TrangThai], [KhuVuc], [TenTrangThai], [TenKhuVuc]) VALUES (5, N'2', N'Lưu Thế Việt', N'111111111', N'Sinh viên', N'Hà Nam', NULL, N'12000000', N'600000', 1, 6, 2021, N'', 2, N'35', N'Chưa đóng', N'Tỉnh Hà Nam')
INSERT [dbo].[DanhSachThuThue] ([ID], [SoThue], [HoTen], [SoCMND], [ThongTin], [DiaChi], [ThoiGianDongThue], [ThuNhapTinhThue], [SoTienDongThue], [BacThue], [ThangDong], [NamDong], [UpdateBy], [TrangThai], [KhuVuc], [TenTrangThai], [TenKhuVuc]) VALUES (6, N'2', N'Lưu Thế Việt', N'111111111', N'Sinh viên', N'Hà Nam', NULL, N'12000000', N'600000', 1, 7, 2021, N'', 2, N'35', N'Chưa đóng', N'Tỉnh Hà Nam')
INSERT [dbo].[DanhSachThuThue] ([ID], [SoThue], [HoTen], [SoCMND], [ThongTin], [DiaChi], [ThoiGianDongThue], [ThuNhapTinhThue], [SoTienDongThue], [BacThue], [ThangDong], [NamDong], [UpdateBy], [TrangThai], [KhuVuc], [TenTrangThai], [TenKhuVuc]) VALUES (7, N'3', N'Nguyễn Quý Minh', N'999999999', N'Sinh viên', N'Đan Phượng', CAST(N'2021-05-02' AS Date), N'9000000', N'480000', 1, 5, 2021, N'', 1, N'1', N'Đã đóng', N'Thành phố Hà Nội')
SET IDENTITY_INSERT [dbo].[DanhSachThuThue] OFF
GO
SET IDENTITY_INSERT [dbo].[Tinh_Thanh] ON 

INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (1, 1, N'Thành phố Hà Nội', N'Thành phố Trung ương')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (2, 2, N'Tỉnh Hà Giang', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (3, 4, N'Tỉnh Cao Bằng', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (4, 6, N'Tỉnh Bắc Kạn', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (5, 8, N'Tỉnh Tuyên Quang', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (6, 10, N'Tỉnh Lào Cai', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (7, 11, N'Tỉnh Điện Biên', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (8, 12, N'Tỉnh Lai Châu', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (9, 14, N'Tỉnh Sơn La', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (10, 15, N'Tỉnh Yên Bái', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (11, 17, N'Tỉnh Hòa Bình', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (12, 19, N'Tỉnh Thái Nguyên', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (13, 20, N'Tỉnh Lạng Sơn', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (14, 22, N'Tỉnh Quảng Ninh', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (15, 24, N'Tỉnh Bắc Giang', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (16, 25, N'Tỉnh Phú Thọ', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (17, 26, N'Tỉnh Vĩnh Phúc', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (18, 27, N'Tỉnh Bắc Ninh', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (19, 30, N'Tỉnh Hải Dương', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (20, 31, N'Thành phố Hải Phòng', N'Thành phố Trung ương')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (21, 33, N'Tỉnh Hưng Yên', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (22, 34, N'Tỉnh Thái Bình', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (23, 35, N'Tỉnh Hà Nam', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (24, 36, N'Tỉnh Nam Định', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (25, 37, N'Tỉnh Ninh Bình', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (26, 38, N'Tỉnh Thanh Hóa', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (27, 40, N'Tỉnh Nghệ An', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (28, 42, N'Tỉnh Hà Tĩnh', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (29, 44, N'Tỉnh Quảng Bình', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (30, 45, N'Tỉnh Quảng Trị', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (31, 46, N'Tỉnh Thừa Thiên Huế', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (32, 48, N'Thành phố Đà Nẵng', N'Thành phố Trung ương')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (33, 49, N'Tỉnh Quảng Nam', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (34, 51, N'Tỉnh Quảng Ngãi', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (35, 52, N'Tỉnh Bình Định', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (36, 54, N'Tỉnh Phú Yên', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (37, 56, N'Tỉnh Khánh Hòa', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (38, 58, N'Tỉnh Ninh Thuận', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (39, 60, N'Tỉnh Bình Thuận', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (40, 62, N'Tỉnh Kon Tum', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (41, 64, N'Tỉnh Gia Lai', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (42, 66, N'Tỉnh Đắk Lắk', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (43, 67, N'Tỉnh Đắk Nông', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (44, 68, N'Tỉnh Lâm Đồng', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (45, 70, N'Tỉnh Bình Phước', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (46, 72, N'Tỉnh Tây Ninh', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (47, 74, N'Tỉnh Bình Dương', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (48, 75, N'Tỉnh Đồng Nai', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (49, 77, N'Tỉnh Bà Rịa - Vũng Tàu', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (50, 79, N'Thành phố Hồ Chí Minh', N'Thành phố Trung ương')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (51, 80, N'Tỉnh Long An', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (52, 82, N'Tỉnh Tiền Giang', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (53, 83, N'Tỉnh Bến Tre', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (54, 84, N'Tỉnh Trà Vinh', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (55, 86, N'Tỉnh Vĩnh Long', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (56, 87, N'Tỉnh Đồng Tháp', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (57, 89, N'Tỉnh An Giang', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (58, 91, N'Tỉnh Kiên Giang', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (59, 92, N'Thành phố Cần Thơ', N'Thành phố Trung ương')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (60, 93, N'Tỉnh Hậu Giang', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (61, 94, N'Tỉnh Sóc Trăng', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (62, 95, N'Tỉnh Bạc Liêu', N'Tỉnh')
INSERT [dbo].[Tinh_Thanh] ([Id], [MaTP], [TenTinh], [Cap]) VALUES (63, 96, N'Tỉnh Cà Mau', N'Tỉnh')
SET IDENTITY_INSERT [dbo].[Tinh_Thanh] OFF
GO
